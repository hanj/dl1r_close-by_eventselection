#include "EventSelection/HistNameSvc.h"

#include "TError.h"

HistNameSvc::HistNameSvc() {
  reset(true);
  m_name.reserve(300);
}

void HistNameSvc::reset(bool resetSample) {
  if (resetSample) {
    set_sample("None");
  }
  set_nVRJet(-1);
  set_description("SR");
  set_variation("Nominal");
  set_leptonFlav("");
  set_collectionPeriod("");
  set_doTruthStudies(false);
  set_passChi2(false);
  set_fullMatch(false);
  set_bMatch(false);
  set_isB(false);
  set_tagged(-1);
  set_truthLabel("NA"); //NA means not do truth labeling, notruth means data if fo truth labeling
}
// name convention:
// hist_process_channel_region_tag_var, process=dsid, or {ttbar, diboson, vjets, data, sitop}
std::string HistNameSvc::getFullHistName(const std::string& variable) {

  // TODO cache sub strings for speed up

  m_name.clear();

  // SET IF NOMINAL OR SYS
  if (!m_isNominal) {

    if(!m_doOneSysDir) {
      m_name += "Sys";
      m_name += m_variation;
    } else m_name += "Systematics";
    
    m_name += "/";
  }

  //start with hist
  m_name += "hist_";

  // SAMPLE NAME
  m_name += m_sample;

  // append event flavour (only used if it's w+jets or z+jets)
  // if (m_useEventFlav) {
  //   appendEventFlavour(m_name);
  // }

  //which data taking period?
  // m_name += "_";
  // m_name += m_collectionPeriod;

  //muon or electron channel?
  m_name += "_";
  m_name += m_leptonFlav;

  //signal region or control region?
  m_name += "_";
  m_name += m_description;

  //option to split according to jet multiplicity
  if(m_nVRJet != -1){
    if(m_nVRJet == 2){
      m_name += "_2VR";
    }else if(m_nVRJet >= 3){
      m_name += "_3pVR";
    }else{
      m_name += "_1mVR";
    }
  }

  //truth options
  if(m_doTruthStudies){
    if(m_passChi2){ m_name += "_passChi2";}
    else{ m_name += "_NpassChi2";}
    
    if(m_fullMatch){ m_name += "_fullMatch"; }
    else{ m_name += "_NfullMatch"; }

    if(m_bMatch){ m_name += "_bMatch"; }
    else{ m_name += "_NbMatch"; }

    if(m_isB){ m_name += "_isB"; }
    else{m_name += "_NisB"; }
  }

  //variable name
  m_name += "_";
  m_name += variable;


  if (!m_isNominal) {
    m_name += "_Sys";
    m_name += m_variation;
  }

  return m_name;
}

void HistNameSvc::set_sample(const std::string& sample) {
  // std::cout << "Setting sample name (mc): " << sample << std::endl;
  m_sample = sample;
  return;
}

void HistNameSvc::set_variation(const std::string& variation) {
  m_variation = variation;
  m_isNominal = (variation == "Nominal");
}

void HistNameSvc::set_isNominal() {
  set_variation("Nominal");
}

void HistNameSvc::set_doOneSysDir(bool doOneSysDir) {
  m_doOneSysDir = doOneSysDir;
}
