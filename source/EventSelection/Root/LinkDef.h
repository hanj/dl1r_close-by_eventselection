#include "EventSelection/EventSelection.h"
#include "EventSelection/InputTree.h"
// #include "EventSelection/NeutrinoBuilder.h"
// #include "EventSelection/Chi2_minimization.h"
#include "EventSelection/CrossSectionDatabase.h"
#include "EventSelection/ConfigFieldBase.h"
#include "EventSelection/ConfigField.h"
#include "EventSelection/ConfigStore.h"
// #include "EventSelection/MVATree.h"
// #include "EventSelection/AssignmentTree.h"

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif


#ifdef __CINT__
#pragma link C++ class EventSelection+;
#pragma link C++ class InputTree+;
// #pragma link C++ class NeutrinoBuilder+;
#pragma link C++ class Chi2_minimization+;
#pragma link C++ class CrossSectionDatabase+;
// #pragma link C++ class MVATree+;
// #pragma link C++ class AssignmentTree+;

#pragma link C++ class ConfigFieldBase+;
#pragma link C++ class ConfigField<bool>+;
#pragma link C++ class ConfigField<int>+;
#pragma link C++ class ConfigField<float>+;
#pragma link C++ class ConfigField<double>+;
#pragma link C++ class ConfigField<std::string>+;
#pragma link C++ class ConfigField<std::vector<bool> >+;
#pragma link C++ class ConfigField<std::vector<int> >+;
#pragma link C++ class ConfigField<std::vector<float> >+;
#pragma link C++ class ConfigField<std::vector<double> >+;
#pragma link C++ class ConfigField<std::vector<std::string> >+;
#pragma link C++ class ConfigStore+;


#endif
