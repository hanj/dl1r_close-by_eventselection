#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include <dirent.h>
#include <cstdlib>
// #include <assert.h>

#include "EventSelection/CrossSectionDatabase.h"

void CrossSectionDatabase::Load(std::string& path)
{
  const std::string wd = "$WorkDir_DIR";
  const std::string rp = std::getenv("WorkDir_DIR");
  
  size_t index = 0;
  index = path.find(wd, index);

  if(index != std::string::npos){
    path.replace(index, wd.length(), rp.c_str());
  }
  
        DIR* dp = opendir(path.c_str());
	if (dp) {
		struct dirent* de = nullptr;
		while ((de = readdir(dp)) != NULL) {
		  //std::cout << path + "/" + de->d_name << std::endl;
		  LoadFile(path + "/" + de->d_name);
		}
	}
	else {
		LoadFile(path);
	}
}

void CrossSectionDatabase::LoadFile(const std::string& path)
{//buggy! fixed.
	// std::cout << "Load XS databse from: " << path << std::endl;
	std::ifstream file(path);
	if(!file.is_open()){
		std::cerr << "Load XS databse FAILED!! " << path << std::endl;
		abort();
	}
	std::string line;
	while (std::getline(file, line)) {
		// std::cout << "XS line: " << line << std::endl;
		if (line.empty() || line[0] == '#')
			continue;

		std::stringstream sstr(line);
		Entry entry;
	        const bool ok = static_cast<bool>(sstr >> entry.dsid >> entry.name >> entry.events_mc15 >> entry.events_mc16a >> entry.events_mc16c >> entry.events_mc16d >> entry.events_mc16e >> entry.sampleLumi >> entry.amiXs >> entry.filtEff >> entry.higherOrderXs >> entry.kFactor >> entry.xs);

		// POSSIBLE PRINTOUTS TO DEBUG
		// std::cout << "Parsed values: " << std::endl;
		// std::cout << " - dsid: " << entry.dsid << std::endl;
		// std::cout << " - name: " << entry.name << std::endl;
		// std::cout << " - events mc15: " << entry.events_mc15 << std::endl;
		// std::cout << " - events mc16a: " << entry.events_mc16a << std::endl;
		// std::cout << " - events mc16c: " << entry.events_mc16c << std::endl;
		// std::cout << " - events mc16d: " << entry.events_mc16d << std::endl;
		// std::cout << " - events mc16e: " << entry.events_mc16e << std::endl;
		// std::cout << " - sampleLumi: " << entry.sampleLumi << std::endl;
		// std::cout << " - amiXS: " << entry.amiXs << std::endl;
		// std::cout << " - filtEff: " << entry.filtEff << std::endl;
		// std::cout << " - higherOrderXs: " << entry.higherOrderXs << std::endl;
		// std::cout << " - kFactor: " << entry.kFactor << std::endl;
		// std::cout << " - xs: " << entry.xs << std::endl;

		if (ok) {
			database[entry.dsid] = entry;
		}
		else {
			std::cerr << "Error: Failed to parse line:\n";
			std::cerr << line << "\n";
			std::cerr << "In file: " << path << "\n";
		}
	}
	file.close();
}
