//necessary includes
#include "EventSelection/InputTree.h"

//constructor
InputTree::InputTree()   //NTUPLESv1
{

   // Need init the variable??
   // Set object pointer
   ////mc
   mc_generator_weights = NULL;
   weight_bTagSF_DL1_77_eigenvars_B_up = NULL;
   weight_bTagSF_DL1_77_eigenvars_C_up = NULL;
   weight_bTagSF_DL1_77_eigenvars_Light_up = NULL;
   weight_bTagSF_DL1_77_eigenvars_B_down = NULL;
   weight_bTagSF_DL1_77_eigenvars_C_down = NULL;
   weight_bTagSF_DL1_77_eigenvars_Light_down = NULL;
   weight_bTagSF_DL1r_77_eigenvars_B_up = NULL;
   weight_bTagSF_DL1r_77_eigenvars_C_up = NULL;
   weight_bTagSF_DL1r_77_eigenvars_Light_up = NULL;
   weight_bTagSF_DL1r_77_eigenvars_B_down = NULL;
   weight_bTagSF_DL1r_77_eigenvars_C_down = NULL;
   weight_bTagSF_DL1r_77_eigenvars_Light_down = NULL;
   weight_trackjet_bTagSF_DL1_77_eigenvars_B_up = NULL;
   weight_trackjet_bTagSF_DL1_77_eigenvars_C_up = NULL;
   weight_trackjet_bTagSF_DL1_77_eigenvars_Light_up = NULL;
   weight_trackjet_bTagSF_DL1_77_eigenvars_B_down = NULL;
   weight_trackjet_bTagSF_DL1_77_eigenvars_C_down = NULL;
   weight_trackjet_bTagSF_DL1_77_eigenvars_Light_down = NULL;

   weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up = NULL;
   weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up = NULL;
   weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up = NULL;
   weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down = NULL;
   weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down = NULL;
   weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down = NULL;

   weight_perjet_trackjet_bTagSF_DL1r_85 = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down = NULL; 
   weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up = NULL;
   weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down = NULL;

   el_true_type = NULL;
   el_true_origin = NULL;
   el_true_firstEgMotherTruthType = NULL;
   el_true_firstEgMotherTruthOrigin = NULL;
   el_true_firstEgMotherPdgId = NULL;
   el_true_isPrompt = NULL;
   el_true_isChargeFl = NULL;

   mu_true_type = NULL;
   mu_true_origin = NULL;
   mu_true_isPrompt = NULL;

   jet_truthflav = NULL;
   jet_truthPartonLabel = NULL;
   jet_isTrueHS = NULL;
   jet_truthflavExtended = NULL;
   tjet_truthflav = NULL;
   tjet_truthflavExtended = NULL;

   ljet_truthLabel = NULL;

   ////common
   el_pt = NULL;
   el_eta = NULL;
   el_cl_eta = NULL;
   el_phi = NULL;
   el_e = NULL;
   el_charge = NULL;
   el_topoetcone20 = NULL;
   el_ptvarcone20 = NULL;
   el_isTight = NULL;
   el_CF = NULL;
   el_d0sig = NULL;
   el_delta_z0_sintheta = NULL;

   mu_pt = NULL;
   mu_eta = NULL;
   mu_phi = NULL;
   mu_e = NULL;
   mu_charge = NULL;
   mu_topoetcone20 = NULL;
   mu_ptvarcone30 = NULL;
   mu_isTight = NULL;
   mu_d0sig = NULL;
   mu_delta_z0_sintheta = NULL;

   jet_pt = NULL;
   jet_eta = NULL;
   jet_phi = NULL;
   jet_e = NULL;
   jet_mv2c10 = NULL;
   jet_jvt = NULL;
   jet_passfjvt = NULL;

   jet_isbtagged_DL1_77 = NULL;
   jet_isbtagged_DL1r_77 = NULL;
   jet_isbtagged_DL1r_85 = NULL;
   jet_isbtagged_DL1r_70 = NULL;
   jet_isbtagged_DL1r_85 = NULL;
   jet_DL1 = NULL;
   jet_DL1r = NULL;
   jet_DL1rmu = NULL;
   jet_DL1_pu = NULL;
   jet_DL1_pc = NULL;
   jet_DL1_pb = NULL;
   jet_DL1r_pu = NULL;
   jet_DL1r_pc = NULL;
   jet_DL1r_pb = NULL;
   jet_DL1rmu_pu = NULL;
   jet_DL1rmu_pc = NULL;
   jet_DL1rmu_pb = NULL;
   ljet_pt = NULL;
   ljet_eta = NULL;
   ljet_phi = NULL;
   ljet_e = NULL;
   ljet_m = NULL;
   ljet_sd12 = NULL;

   tjet_pt = NULL;
   tjet_eta = NULL;
   tjet_phi = NULL;
   tjet_e = NULL;
   tjet_mv2c00 = NULL;
   tjet_mv2c10 = NULL;
   tjet_mv2c20 = NULL;
   tjet_DL1 = NULL;
   tjet_DL1r = NULL;
   tjet_DL1rmu = NULL;
   tjet_isbtagged_DL1_77 = NULL;
   tjet_isbtagged_DL1r_77 = NULL;
   tjet_isbtagged_DL1r_85 = NULL;
   tjet_isbtagged_DL1r_70 = NULL;
   tjet_isbtagged_DL1r_85 = NULL;
   el_trigMatch_HLT_e60_lhmedium_nod0 = NULL;
   el_trigMatch_HLT_e60_lhmedium = NULL;
   el_trigMatch_HLT_e24_lhmedium_L1EM20VH = NULL;
   el_trigMatch_HLT_e120_lhloose = NULL;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = NULL;
   el_trigMatch_HLT_e300_etcut = NULL;
   el_trigMatch_HLT_e140_lhloose_nod0 = NULL;
   mu_trigMatch_HLT_mu50 = NULL;
   mu_trigMatch_HLT_mu20_iloose_L1MU15 = NULL;
   mu_trigMatch_HLT_mu26_ivarmedium = NULL;
   ljet_SubjetBScore_Top = NULL;
   ljet_SubjetBScore_QCD = NULL;
   ljet_SubjetBScore_Higgs = NULL;
   ljet_Xbb202006_Top = NULL;
   ljet_Xbb202006_QCD = NULL;
   ljet_Xbb202006_Higgs = NULL;
   ljet_m_Xbb2020v3_Top = NULL;
   ljet_m_Xbb2020v3_QCD = NULL;
   ljet_m_Xbb2020v3_Higgs = NULL;

   jet_mass = NULL;
   tjet_raw_index = NULL;
   ljet1_ghostVR_trkjet_m = NULL;
   ljet1_ghostVR_trkjet_pt = NULL;
   ljet1_ghostVR_trkjet_eta = NULL;
   ljet1_ghostVR_trkjet_phi = NULL;
   ljet1_ghostVR_trkjet_e = NULL;
   ljet1_ghostVR_trkjet_DL1r = NULL;
   ljet1_ghostVR_trkjet_DL1r_pu = NULL;
   ljet1_ghostVR_trkjet_DL1r_pc = NULL;
   ljet1_ghostVR_trkjet_DL1r_pb = NULL;
   ljet1_ghostVR_trkjet_truthflav = NULL;
   ljet1_ghostVR_trkjet_truthflavExtended = NULL;
   ljet1_ghostVR_trkjet_raw_index = NULL;
   ljet1_ghostVR_trkjet_index = NULL;
   ljet_ghostVR_indices = NULL;
   ljet_ghostVR_indices_raw = NULL;

   // Set branch addresses and branch pointers
   //   if (!tree) return;
   //   fChain = tree;
   //   fCurrent = -1;
   //   fChain->SetMakeClass(1);

   //Need initialize to default values and nullpointers??

   //truth variables
   /* jet_deltaR = 0;
   m_W1_had_pt = 0;
   m_W1_had_eta = 0;
   m_W1_had_phi = 0;
   m_W1_had_m = 0;
   m_W2_had_pt = 0;
   m_W2_had_eta = 0;
   m_W2_had_phi = 0;
   m_W2_had_m = 0;
   m_b_had_pt = 0;
   m_b_had_eta = 0;
   m_b_had_phi = 0;
   m_b_had_m = 0;
   m_W1_lep_pt = 0;
   m_W1_lep_eta = 0;
   m_W1_lep_phi = 0;
   m_W1_lep_m = 0;
   m_W2_lep_pt = 0;
   m_W2_lep_eta = 0;
   m_W2_lep_phi = 0;
   m_W2_lep_m = 0;
   m_b_lep_pt = 0;
   m_b_lep_eta = 0;
   m_b_lep_phi = 0;
   m_b_lep_m = 0;
   m_b_is_full_had = 0;
   m_b_is_dilep = 0;
*/
   // el_ptvarcone20_TightTTVA_pt1000 = 0;
   // mu_ptvarcone30_TightTTVA_pt1000 = 0;
}

//destructor
InputTree::~InputTree()
{
   //memory management, clean up
}

//setting branch adresses etc.
void InputTree::initialize(TTree *tree, bool isData=false)
{

   //annoying but fast way...
   //1. do common and judge data or mc
   tree->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   tree->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   tree->SetBranchAddress("mu", &mu, &b_mu);
   tree->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   tree->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   tree->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   tree->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   tree->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   tree->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   tree->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   tree->SetBranchAddress("el_e", &el_e, &b_el_e);
   tree->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   tree->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   tree->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   tree->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
   tree->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   tree->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   tree->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   tree->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   tree->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   tree->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   tree->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   tree->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   tree->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   tree->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
   tree->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   tree->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   tree->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   tree->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   tree->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   tree->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   tree->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   tree->SetBranchAddress("jet_isbtagged_DL1_77", &jet_isbtagged_DL1_77, &b_jet_isbtagged_DL1_77);
   tree->SetBranchAddress("jet_isbtagged_DL1r_77", &jet_isbtagged_DL1r_77, &b_jet_isbtagged_DL1r_77);
   tree->SetBranchAddress("jet_isbtagged_DL1r_85", &jet_isbtagged_DL1r_85, &b_jet_isbtagged_DL1r_85);
   tree->SetBranchAddress("jet_isbtagged_DL1r_70", &jet_isbtagged_DL1r_70, &b_jet_isbtagged_DL1r_70);
   tree->SetBranchAddress("jet_isbtagged_DL1r_85", &jet_isbtagged_DL1r_85, &b_jet_isbtagged_DL1r_85);
   tree->SetBranchAddress("jet_DL1", &jet_DL1, &b_jet_DL1);
   tree->SetBranchAddress("jet_DL1r", &jet_DL1r, &b_jet_DL1r);
   tree->SetBranchAddress("jet_DL1rmu", &jet_DL1rmu, &b_jet_DL1rmu);
   tree->SetBranchAddress("jet_DL1_pu", &jet_DL1_pu, &b_jet_DL1_pu);
   tree->SetBranchAddress("jet_DL1_pc", &jet_DL1_pc, &b_jet_DL1_pc);
   tree->SetBranchAddress("jet_DL1_pb", &jet_DL1_pb, &b_jet_DL1_pb);
   tree->SetBranchAddress("jet_DL1r_pu", &jet_DL1r_pu, &b_jet_DL1r_pu);
   tree->SetBranchAddress("jet_DL1r_pc", &jet_DL1r_pc, &b_jet_DL1r_pc);
   tree->SetBranchAddress("jet_DL1r_pb", &jet_DL1r_pb, &b_jet_DL1r_pb);
   tree->SetBranchAddress("jet_DL1rmu_pu", &jet_DL1rmu_pu, &b_jet_DL1rmu_pu);
   tree->SetBranchAddress("jet_DL1rmu_pc", &jet_DL1rmu_pc, &b_jet_DL1rmu_pc);
   tree->SetBranchAddress("jet_DL1rmu_pb", &jet_DL1rmu_pb, &b_jet_DL1rmu_pb);
   tree->SetBranchAddress("ljet_pt", &ljet_pt, &b_ljet_pt);
   tree->SetBranchAddress("ljet_eta", &ljet_eta, &b_ljet_eta);
   tree->SetBranchAddress("ljet_phi", &ljet_phi, &b_ljet_phi);
   tree->SetBranchAddress("ljet_e", &ljet_e, &b_ljet_e);
   tree->SetBranchAddress("ljet_m", &ljet_m, &b_ljet_m);
   tree->SetBranchAddress("tjet_pt", &tjet_pt, &b_tjet_pt);
   tree->SetBranchAddress("tjet_eta", &tjet_eta, &b_tjet_eta);
   tree->SetBranchAddress("tjet_phi", &tjet_phi, &b_tjet_phi);
   tree->SetBranchAddress("tjet_e", &tjet_e, &b_tjet_e);
   tree->SetBranchAddress("tjet_mv2c00", &tjet_mv2c00, &b_tjet_mv2c00);
   tree->SetBranchAddress("tjet_mv2c10", &tjet_mv2c10, &b_tjet_mv2c10);
   tree->SetBranchAddress("tjet_mv2c20", &tjet_mv2c20, &b_tjet_mv2c20);
   tree->SetBranchAddress("tjet_DL1", &tjet_DL1, &b_tjet_DL1);
   tree->SetBranchAddress("tjet_DL1r", &tjet_DL1r, &b_tjet_DL1r);
   tree->SetBranchAddress("tjet_DL1rmu", &tjet_DL1rmu, &b_tjet_DL1rmu);
   tree->SetBranchAddress("tjet_isbtagged_DL1_77", &tjet_isbtagged_DL1_77, &b_tjet_isbtagged_DL1_77);
   tree->SetBranchAddress("tjet_isbtagged_DL1r_77", &tjet_isbtagged_DL1r_77, &b_tjet_isbtagged_DL1r_77);
   tree->SetBranchAddress("tjet_isbtagged_DL1r_85", &tjet_isbtagged_DL1r_85, &b_tjet_isbtagged_DL1r_85);
   tree->SetBranchAddress("tjet_isbtagged_DL1r_70", &tjet_isbtagged_DL1r_70, &b_tjet_isbtagged_DL1r_70);
   tree->SetBranchAddress("tjet_isbtagged_DL1r_85", &tjet_isbtagged_DL1r_85, &b_tjet_isbtagged_DL1r_85);
   tree->SetBranchAddress("met_met", &met_met, &b_met_met);
   tree->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   tree->SetBranchAddress("efatjets", &efatjets, &b_efatjets);
   tree->SetBranchAddress("mufatjets", &mufatjets, &b_mufatjets);
   tree->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   tree->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   tree->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   tree->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   tree->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   tree->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   tree->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   tree->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   tree->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   tree->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   tree->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium, &b_el_trigMatch_HLT_e60_lhmedium);
   tree->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH, &b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
   tree->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose, &b_el_trigMatch_HLT_e120_lhloose);
   tree->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
   tree->SetBranchAddress("el_trigMatch_HLT_e300_etcut", &el_trigMatch_HLT_e300_etcut, &b_el_trigMatch_HLT_e300_etcut);
   tree->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
   tree->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
   tree->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15, &b_mu_trigMatch_HLT_mu20_iloose_L1MU15);
   tree->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
   tree->SetBranchAddress("ljet_SubjetBScore_Top", &ljet_SubjetBScore_Top, &b_ljet_SubjetBScore_Top);
   tree->SetBranchAddress("ljet_SubjetBScore_QCD", &ljet_SubjetBScore_QCD, &b_ljet_SubjetBScore_QCD);
   tree->SetBranchAddress("ljet_SubjetBScore_Higgs", &ljet_SubjetBScore_Higgs, &b_ljet_SubjetBScore_Higgs);
   tree->SetBranchAddress("ljet_Xbb202006_Top", &ljet_Xbb202006_Top, &b_ljet_Xbb202006_Top);
   tree->SetBranchAddress("ljet_Xbb202006_QCD", &ljet_Xbb202006_QCD, &b_ljet_Xbb202006_QCD);
   tree->SetBranchAddress("ljet_Xbb202006_Higgs", &ljet_Xbb202006_Higgs, &b_ljet_Xbb202006_Higgs);
   tree->SetBranchAddress("ljet_m_Xbb2020v3_Top", &ljet_m_Xbb2020v3_Top, &b_ljet_m_Xbb2020v3_Top);
   tree->SetBranchAddress("ljet_m_Xbb2020v3_QCD", &ljet_m_Xbb2020v3_QCD, &b_ljet_m_Xbb2020v3_QCD);
   tree->SetBranchAddress("ljet_m_Xbb2020v3_Higgs", &ljet_m_Xbb2020v3_Higgs, &b_ljet_m_Xbb2020v3_Higgs);
   tree->SetBranchAddress("jet_mass", &jet_mass, &b_jet_mass);
   tree->SetBranchAddress("tjet_raw_index", &tjet_raw_index, &b_tjet_raw_index);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_m", &ljet1_ghostVR_trkjet_m, &b_ljet1_ghostVR_trkjet_m);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_pt", &ljet1_ghostVR_trkjet_pt, &b_ljet1_ghostVR_trkjet_pt);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_eta", &ljet1_ghostVR_trkjet_eta, &b_ljet1_ghostVR_trkjet_eta);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_phi", &ljet1_ghostVR_trkjet_phi, &b_ljet1_ghostVR_trkjet_phi);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_e", &ljet1_ghostVR_trkjet_e, &b_ljet1_ghostVR_trkjet_e);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_DL1r", &ljet1_ghostVR_trkjet_DL1r, &b_ljet1_ghostVR_trkjet_DL1r);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_DL1r_pu", &ljet1_ghostVR_trkjet_DL1r_pu, &b_ljet1_ghostVR_trkjet_DL1r_pu);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_DL1r_pc", &ljet1_ghostVR_trkjet_DL1r_pc, &b_ljet1_ghostVR_trkjet_DL1r_pc);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_DL1r_pb", &ljet1_ghostVR_trkjet_DL1r_pb, &b_ljet1_ghostVR_trkjet_DL1r_pb);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_truthflav", &ljet1_ghostVR_trkjet_truthflav, &b_ljet1_ghostVR_trkjet_truthflav);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_truthflavExtended", &ljet1_ghostVR_trkjet_truthflavExtended, &b_ljet1_ghostVR_trkjet_truthflavExtended);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_raw_index", &ljet1_ghostVR_trkjet_raw_index, &b_ljet1_ghostVR_trkjet_raw_index);
   tree->SetBranchAddress("ljet1_ghostVR_trkjet_index", &ljet1_ghostVR_trkjet_index, &b_ljet1_ghostVR_trkjet_index);
   tree->SetBranchAddress("ljet_ghostVR_indices", &ljet_ghostVR_indices, &b_ljet_ghostVR_indices);
   tree->SetBranchAddress("ljet_ghostVR_indices_raw", &ljet_ghostVR_indices_raw, &b_ljet_ghostVR_indices_raw);

   //2.do data/mc
   if (isData)
   {
      tree->SetBranchAddress("mu_original_xAOD", &mu_original_xAOD, &b_mu_original_xAOD);
      tree->SetBranchAddress("mu_actual_original_xAOD", &mu_actual_original_xAOD, &b_mu_actual_original_xAOD);
   }
   else
   {
      tree->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
      tree->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
      tree->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
      tree->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF", &weight_globalLeptonTriggerSF, &b_weight_globalLeptonTriggerSF);
      tree->SetBranchAddress("weight_oldTriggerSF", &weight_oldTriggerSF, &b_weight_oldTriggerSF);
      tree->SetBranchAddress("weight_bTagSF_DL1_77", &weight_bTagSF_DL1_77, &b_weight_bTagSF_DL1_77);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77, &b_weight_bTagSF_DL1r_77);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77", &weight_trackjet_bTagSF_DL1_77, &b_weight_trackjet_bTagSF_DL1_77);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85", &weight_trackjet_bTagSF_DL1r_85, &b_weight_trackjet_bTagSF_DL1r_85);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85", &weight_perjet_trackjet_bTagSF_DL1r_85, &b_weight_perjet_trackjet_bTagSF_DL1r_85);
      tree->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
      tree->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
      tree->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
      tree->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
      tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_UP", &weight_globalLeptonTriggerSF_EL_Trigger_UP, &b_weight_globalLeptonTriggerSF_EL_Trigger_UP);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &weight_globalLeptonTriggerSF_EL_Trigger_DOWN, &b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
      tree->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
      tree->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_UP", &weight_oldTriggerSF_EL_Trigger_UP, &b_weight_oldTriggerSF_EL_Trigger_UP);
      tree->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_DOWN", &weight_oldTriggerSF_EL_Trigger_DOWN, &b_weight_oldTriggerSF_EL_Trigger_DOWN);
      tree->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_UP", &weight_oldTriggerSF_MU_Trigger_STAT_UP, &b_weight_oldTriggerSF_MU_Trigger_STAT_UP);
      tree->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_DOWN", &weight_oldTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN);
      tree->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_UP", &weight_oldTriggerSF_MU_Trigger_SYST_UP, &b_weight_oldTriggerSF_MU_Trigger_SYST_UP);
      tree->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_DOWN", &weight_oldTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
      tree->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
      tree->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
      tree->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
      tree->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
      tree->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
      tree->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeID", &weight_indiv_SF_EL_ChargeID, &b_weight_indiv_SF_EL_ChargeID);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weight_indiv_SF_EL_ChargeID_UP, &b_weight_indiv_SF_EL_ChargeID_UP);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weight_indiv_SF_EL_ChargeID_DOWN, &b_weight_indiv_SF_EL_ChargeID_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID", &weight_indiv_SF_EL_ChargeMisID, &b_weight_indiv_SF_EL_ChargeMisID);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weight_indiv_SF_EL_ChargeMisID_STAT_UP, &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weight_indiv_SF_EL_ChargeMisID_STAT_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weight_indiv_SF_EL_ChargeMisID_SYST_UP, &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
      tree->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weight_indiv_SF_EL_ChargeMisID_SYST_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
      tree->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
      tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
      tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
      tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
      tree->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
      tree->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_eigenvars_B_up", &weight_bTagSF_DL1_77_eigenvars_B_up, &b_weight_bTagSF_DL1_77_eigenvars_B_up);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_eigenvars_C_up", &weight_bTagSF_DL1_77_eigenvars_C_up, &b_weight_bTagSF_DL1_77_eigenvars_C_up);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_eigenvars_Light_up", &weight_bTagSF_DL1_77_eigenvars_Light_up, &b_weight_bTagSF_DL1_77_eigenvars_Light_up);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_eigenvars_B_down", &weight_bTagSF_DL1_77_eigenvars_B_down, &b_weight_bTagSF_DL1_77_eigenvars_B_down);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_eigenvars_C_down", &weight_bTagSF_DL1_77_eigenvars_C_down, &b_weight_bTagSF_DL1_77_eigenvars_C_down);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_eigenvars_Light_down", &weight_bTagSF_DL1_77_eigenvars_Light_down, &b_weight_bTagSF_DL1_77_eigenvars_Light_down);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_extrapolation_up", &weight_bTagSF_DL1_77_extrapolation_up, &b_weight_bTagSF_DL1_77_extrapolation_up);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_extrapolation_down", &weight_bTagSF_DL1_77_extrapolation_down, &b_weight_bTagSF_DL1_77_extrapolation_down);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_extrapolation_from_charm_up", &weight_bTagSF_DL1_77_extrapolation_from_charm_up, &b_weight_bTagSF_DL1_77_extrapolation_from_charm_up);
      tree->SetBranchAddress("weight_bTagSF_DL1_77_extrapolation_from_charm_down", &weight_bTagSF_DL1_77_extrapolation_from_charm_down, &b_weight_bTagSF_DL1_77_extrapolation_from_charm_down);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_B_up", &weight_bTagSF_DL1r_77_eigenvars_B_up, &b_weight_bTagSF_DL1r_77_eigenvars_B_up);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_C_up", &weight_bTagSF_DL1r_77_eigenvars_C_up, &b_weight_bTagSF_DL1r_77_eigenvars_C_up);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_Light_up", &weight_bTagSF_DL1r_77_eigenvars_Light_up, &b_weight_bTagSF_DL1r_77_eigenvars_Light_up);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_B_down", &weight_bTagSF_DL1r_77_eigenvars_B_down, &b_weight_bTagSF_DL1r_77_eigenvars_B_down);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_C_down", &weight_bTagSF_DL1r_77_eigenvars_C_down, &b_weight_bTagSF_DL1r_77_eigenvars_C_down);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_Light_down", &weight_bTagSF_DL1r_77_eigenvars_Light_down, &b_weight_bTagSF_DL1r_77_eigenvars_Light_down);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_up", &weight_bTagSF_DL1r_77_extrapolation_up, &b_weight_bTagSF_DL1r_77_extrapolation_up);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_down", &weight_bTagSF_DL1r_77_extrapolation_down, &b_weight_bTagSF_DL1r_77_extrapolation_down);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_from_charm_up", &weight_bTagSF_DL1r_77_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_77_extrapolation_from_charm_up);
      tree->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_from_charm_down", &weight_bTagSF_DL1r_77_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_77_extrapolation_from_charm_down);

      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_eigenvars_B_up", &weight_trackjet_bTagSF_DL1_77_eigenvars_B_up, &b_weight_trackjet_bTagSF_DL1_77_eigenvars_B_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_eigenvars_C_up", &weight_trackjet_bTagSF_DL1_77_eigenvars_C_up, &b_weight_trackjet_bTagSF_DL1_77_eigenvars_C_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_eigenvars_Light_up", &weight_trackjet_bTagSF_DL1_77_eigenvars_Light_up, &b_weight_trackjet_bTagSF_DL1_77_eigenvars_Light_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_eigenvars_B_down", &weight_trackjet_bTagSF_DL1_77_eigenvars_B_down, &b_weight_trackjet_bTagSF_DL1_77_eigenvars_B_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_eigenvars_C_down", &weight_trackjet_bTagSF_DL1_77_eigenvars_C_down, &b_weight_trackjet_bTagSF_DL1_77_eigenvars_C_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_eigenvars_Light_down", &weight_trackjet_bTagSF_DL1_77_eigenvars_Light_down, &b_weight_trackjet_bTagSF_DL1_77_eigenvars_Light_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_extrapolation_up", &weight_trackjet_bTagSF_DL1_77_extrapolation_up, &b_weight_trackjet_bTagSF_DL1_77_extrapolation_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_extrapolation_down", &weight_trackjet_bTagSF_DL1_77_extrapolation_down, &b_weight_trackjet_bTagSF_DL1_77_extrapolation_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_up", &weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_up, &b_weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_down", &weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_down, &b_weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_down);


      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up", &weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up, &b_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up", &weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up, &b_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up", &weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up, &b_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down", &weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down, &b_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down", &weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down, &b_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down", &weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down, &b_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_extrapolation_up", &weight_trackjet_bTagSF_DL1r_85_extrapolation_up, &b_weight_trackjet_bTagSF_DL1r_85_extrapolation_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_extrapolation_down", &weight_trackjet_bTagSF_DL1r_85_extrapolation_down, &b_weight_trackjet_bTagSF_DL1r_85_extrapolation_down);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up", &weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up, &b_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up);
      tree->SetBranchAddress("weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down", &weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down, &b_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down);



      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up", &weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up, &b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up", &weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up, &b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up", &weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up, &b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down", &weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down, &b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down", &weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down, &b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down", &weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down, &b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up", &weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up, &b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down", &weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down, &b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up", &weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up, &b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up);
      tree->SetBranchAddress("weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down", &weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down, &b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down);


      tree->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
      tree->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
      tree->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
      tree->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
      tree->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
      tree->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
      tree->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
      tree->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl);
      tree->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
      tree->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
      tree->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
      tree->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
      tree->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
      tree->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
      tree->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended);
      tree->SetBranchAddress("tjet_truthflav", &tjet_truthflav, &b_tjet_truthflav);
      tree->SetBranchAddress("tjet_truthflavExtended", &tjet_truthflavExtended, &b_tjet_truthflavExtended);
      tree->SetBranchAddress("ljet_truthLabel", &ljet_truthLabel, &b_ljet_truthLabel);
   }
}
