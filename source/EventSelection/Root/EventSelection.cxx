//Cleaned version: for all features from previous frawwork, see xxx.cxx~
#include "EventLoop/Job.h"
#include "EventLoop/Worker.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/OutputStream.h"
#include "EventSelection/EventSelection.h"
#include "TF1.h"
#include <fstream>
#include <algorithm>
#include "TRegexp.h"

// this is needed to distribute the algorithm to the workers
ClassImp(EventSelection)

    EventSelection::EventSelection() : m_eventCounter(0),
                                       m_elTrigMatch(0),
                                       m_muTrigMatch(0),
                                       m_jetIndex_boosted_top(0),
                                       m_isSignal(-1),
                                       weight_perAssociatedVR_DL1r85(),
                                        isVHbbTagged_perAssociatedVR(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_up(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_down(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up(),
                                        weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down(),
                                        Atjet_DL1r()

{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_tree = 0;
  m_sumOfWeightsProvider = 0;
  m_histSvc = 0;
  m_histNameSvc = 0;
  m_config = 0;
  m_cf_15_el_mc = 0;
  m_cf_15_mu_mc = 0;
  m_cf_16_el_mc = 0;
  m_cf_16_mu_mc = 0;
  m_cf_17_el_mc = 0;
  m_cf_17_mu_mc = 0;
  m_cf_15_el_dat = 0;
  m_cf_15_mu_dat = 0;
  m_cf_16_el_dat = 0;
  m_cf_16_mu_dat = 0;
  m_cf_17_el_dat = 0;
  m_cf_17_mu_dat = 0;
  m_probetree = 0;

  m_sample = -1;
  std::cout << "Constructor called" << std::endl;
  std::cout << "ConfigStore" << m_config << std::endl;

  // //BM: Urgent -> THIS NEEDS INVESTIGATIONS
  // //why is this needed?
  // std::string configPath = "data/EventSelection/selectEvents.cfg"; //figure out what to put here
  // m_config = ConfigStore::createStore(configPath);
  //We set the config from runXXX tool, after initilize
  
  std::cout << "Finished constructor" << std::endl;
}

EventSelection::~EventSelection()
{
}

EL::StatusCode EventSelection::setupJob(EL::Job &job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  Info("setupJob()", "Setting up job.");

  if (!m_config)
  {
    Error("setupJob()", "ConfigStore not setup! Remember to set it via setConfig()!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
} // setupJob

EL::StatusCode EventSelection::histInitialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  Info("EventSelection::histInitialize()", "Initializing histograms.");
  std::cout << "ConfigStore" << m_config << std::endl;

  // histogram manager
  m_histSvc = new HistSvc();
  m_histNameSvc = new HistNameSvc();
  m_histSvc->SetNameSvc(m_histNameSvc);
  m_histSvc->SetWeightSysts(&m_weightSysts);

  bool fillHists = true;
  //TODO: Create a config file and add this as an option
  m_histSvc->SetFillHists(fillHists);

  //Cutflow histograms (TODO: maybe also a nicer way to do this)
  m_cf_15_el_mc = new TH1D("h_cutflow_2015_el_mc", "", 20, -0.5, 19.5);
  m_cf_15_mu_mc = new TH1D("h_cutflow_2015_mu_mc", "", 20, -0.5, 19.5);
  m_cf_16_el_mc = new TH1D("h_cutflow_2016_el_mc", "", 20, -0.5, 19.5);
  m_cf_16_mu_mc = new TH1D("h_cutflow_2016_mu_mc", "", 20, -0.5, 19.5);
  m_cf_17_el_mc = new TH1D("h_cutflow_2017_el_mc", "", 20, -0.5, 19.5);
  m_cf_17_mu_mc = new TH1D("h_cutflow_2017_mu_mc", "", 20, -0.5, 19.5);
  m_cf_18_el_mc = new TH1D("h_cutflow_2018_el_mc", "", 20, -0.5, 19.5);
  m_cf_18_mu_mc = new TH1D("h_cutflow_2018_mu_mc", "", 20, -0.5, 19.5);

  m_cf_15_el_dat = new TH1D("h_cutflow_2015_el_dat", "", 20, -0.5, 19.5);
  m_cf_15_mu_dat = new TH1D("h_cutflow_2015_mu_dat", "", 20, -0.5, 19.5);
  m_cf_16_el_dat = new TH1D("h_cutflow_2016_el_dat", "", 20, -0.5, 19.5);
  m_cf_16_mu_dat = new TH1D("h_cutflow_2016_mu_dat", "", 20, -0.5, 19.5);
  m_cf_17_el_dat = new TH1D("h_cutflow_2017_el_dat", "", 20, -0.5, 19.5);
  m_cf_17_mu_dat = new TH1D("h_cutflow_2017_mu_dat", "", 20, -0.5, 19.5);
  m_cf_18_el_dat = new TH1D("h_cutflow_2018_el_dat", "", 20, -0.5, 19.5);
  m_cf_18_mu_dat = new TH1D("h_cutflow_2018_mu_dat", "", 20, -0.5, 19.5);

  wk()->addOutput(m_cf_15_el_mc);
  wk()->addOutput(m_cf_15_mu_mc);
  wk()->addOutput(m_cf_16_el_mc);
  wk()->addOutput(m_cf_16_mu_mc);
  wk()->addOutput(m_cf_17_el_mc);
  wk()->addOutput(m_cf_17_mu_mc);
  wk()->addOutput(m_cf_18_el_mc);
  wk()->addOutput(m_cf_18_mu_mc);

  wk()->addOutput(m_cf_15_el_dat);
  wk()->addOutput(m_cf_15_mu_dat);
  wk()->addOutput(m_cf_16_el_dat);
  wk()->addOutput(m_cf_16_mu_dat);
  wk()->addOutput(m_cf_17_el_dat);
  wk()->addOutput(m_cf_17_mu_dat);
  wk()->addOutput(m_cf_18_el_dat);
  wk()->addOutput(m_cf_18_mu_dat);

  //ProbeJet trees (BM: there should be a more elegant way to do this)
  m_probetree = new ProbeJetTree();
  m_probetree->CreateTree("probetree");

  wk()->addOutput(m_probetree->tree);

  return EL::StatusCode::SUCCESS;

} // histInitialize

EL::StatusCode EventSelection::execute()
{
  //<custom debug flag>
  bool Ldebug = false;
  if (m_config->get<int>("maxEvents")>0){ //avoid debug in -1 runall mode
    m_config->getif<bool>("Ldebug",Ldebug); //in case this is missing
  }
  //</custom debug flag>

  m_tree->GetEntry(wk()->treeEntry()); //get next tree entry
  //let the user have a rough idea how many events are already processed
  if (Ldebug || (m_eventCounter % 10000) == 0)
  {
    Info("EventSelection::execute()", "Processing event number = %li", m_eventCounter);
  }
  m_eventCounter++;

  
  

  ////////////////////////////////////////////////////
  // HistNameSvc settings
  ////////////////////////////////////////////////////
  // to record the right name for current event, e/mu/...
  m_histNameSvc->reset();
  m_histNameSvc->set_variation("Nominal"); //only run nominal
  m_histNameSvc->set_doOneSysDir(true);    //put all syst in one directory, we do not run systematics yet anyway ;)

  ////////////////////////////////////////////////////
  //get the right event weight (first bc of cutflow)
  ////////////////////////////////////////////////////
  m_eventWeight = getEventWeight();

  m_elTrigMatch = -1;
  m_muTrigMatch = -1;

  fillCutflow(0); //dummy?

  ////////////////////////////////////////////////////
  //do the trigger matching
  ////////////////////////////////////////////////////
  if (!passTrigger())
  {
    Warning("EventSelection::execute()", "Did not pass trigger matching");
    return EL::StatusCode::SUCCESS;
  }

  fillCutflow(1);

  //check whether the stored leptons correspond to the triggered ones
  m_elTrigMatch = checkElectronMatch();
  m_muTrigMatch = checkMuonMatch();

  if ((m_elTrigMatch + m_muTrigMatch) == 0)
  {
    Warning("EventSelection::execute()", "No lepton stored that is trigger matched");
    return EL::StatusCode::SUCCESS;
  }
  if ((m_elTrigMatch + m_muTrigMatch) > 1)
  {
    Warning("EventSelection::execute()", "Multiple leptons trigger matched");
    return EL::StatusCode::SUCCESS;
  }

  fillCutflow(2); //first valid fill since cutflow histo name is not specify before

  //update histogram name with lepton flavor and collection year
  //TODO: this should be steerable with config flags in the config file
  if (m_elTrigMatch == 1)
    m_histNameSvc->set_leptonFlav("el");
  if (m_muTrigMatch == 1)
    m_histNameSvc->set_leptonFlav("mu");
  m_histNameSvc->set_collectionPeriod(std::to_string(collectionYear()));

  ////////////////////////////////////////////////////
  //get the physical objects
  ////////////////////////////////////////////////////

  //get the lepton candidate (the leading lepton)
  getLepton();
  //get the missing transverse energt
  getMET();
  //get the jet collection
  getJets();
  //get all VR track jets
  getVRJets();
  //get the large R jet collection
  getLargeRJets();
  //NINI get all associated VR track jets

  fillCutflow(3);

  //split the histograms according to jet multiplicity if specified in the config file
  bool doNVRJetSplit = m_config->get<bool>("doNVRJetSplit");
  if (doNVRJetSplit)
  {
    // m_histNameSvc->set_nVRJet(m_jets.size());
    //m_histNameSvc->set_nVRJet(m_input.ljet1_ghostVR_trkjet_raw_index->size());
    int probInd = getBoostedTopJet();
    //m_histNameSvc->set_nVRJet(m_input.ljet_ghostVR_indices[probInd].size());
    m_histNameSvc->set_nVRJet(m_input.ljet_ghostVR_indices->at(probInd).size());
  }
  m_histNameSvc->set_description("SRv1");

  //check which jets are b-tagged (unfortunately this is needed in this ugly format by the chi2 thingy, change if time)
  std::vector<TLorentzVector *> jetPointers(m_jets.size());
  m_jets_isTagged.clear();
  m_jets_isTagged.reserve(m_jets.size());

  for (size_t i = 0; i < m_jets.size(); i++)
  {
    jetPointers[i] = &m_jets[i];
    const bool isTaggedProd = m_input.jet_isbtagged_DL1r_77->at(i);
    if(Ldebug){
      std::cout<<"Small-R jet "<< i <<" taggedProd: "<<(isTaggedProd?"Y":"N")<<std::endl;
    }
    m_jets_isTagged.push_back(isTaggedProd);

  }

  ////////////////////////////////////////////////////
  //Chi-squared minimization to get the assignment
  ////////////////////////////////////////////////////
  // L: now disable chi2 and just use simple matching
  // set Getxxx() function
  // m_chi2_ok = m_chi2.Reconstruct_FullTTbarDecay(&m_lepton, &jetPointers, &m_met, m_jetIndex_q1_W, m_jetIndex_q2_W, m_jetIndex_b_had, m_jetIndex_b_lep, ign1, m_chi2ming1H, m_chi2ming1L, m_chi2min_total, m_jets_isTagged);

  m_jetIndex_boosted_top = -1;
  m_jetIndex_boosted_b_lep = -1;
  m_probeIndex=-1;

  ////////////////////////////////////////////////////
  //do the preselection
  ////////////////////////////////////////////////////
    if (!passBoostedPreselection())
    {
      return EL::StatusCode::SUCCESS;
    }

  ////////////////////////////////////////////////////
  //Modify the event weight to correct possible data/MC shapes
  ////////////////////////////////////////////////////
  //--> correct shape by using a parametric function
  //ttbar_pt_correction();
  //--> correct shape bin-by-bin
  //ttbar_pt_binwise_correction();

  //////////////////////////////////////////////////////
  //Optional assignment studies for the PJT
  //////////////////////////////////////////////////////
  // if (writeAssignmentTree)
  // {
  //   // fill_assignment_tree();//L: debug
  //   return EL::StatusCode::SUCCESS;
  // }

  //fill chi2 control plots before the potential mva score cut
  // fillPrecutChi2Histos(); //L: not fill now for debuging

  ////////////////////////////////////////////////////
  //Throw away events that do not pass the bdt cut (can also be done in calibration code)
  ////////////////////////////////////////////////////

  //fillCutflow(12);
  fillCutflow(13);
  //cutflow >= 15 is used for debuging!

  ////////////////////////////////////////////////////
  //write output tree and histograms
  ////////////////////////////////////////////////////
  //write output histograms
  if (fillHistograms()!=EL::StatusCode::SUCCESS) return EL::StatusCode::FAILURE;
  //write output tree (probe jet tree for deriving the SF later)
  if (fillBoostedProbeJetTree()!=EL::StatusCode::SUCCESS) return EL::StatusCode::FAILURE;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  // std::cout << "fileExecute Input file is " << wk()->inputFile()->GetName() << std::endl;

  return EL::StatusCode::SUCCESS;
} // fileExecute

EL::StatusCode EventSelection::changeInput(bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  Info("EventSelection::changeInput()", "Change Input");

  // It seems PROOF-lite needs the file initialization here
  std::cout << "ConfigStore" << m_config << std::endl;
  if (!m_config)
  {
    Warning("setupJob()", "ConfigStore not setup! Remember to set it via setConfig()!");
  }

  //Is it a signal event (information needed later for calibration)
  TString fileName = wk()->inputFile()->GetName();
  std::cout << "Input: " << fileName << std::endl;
  Warning("changeInput()", "Samples type matching via to full path name thus might be dangerous!");
  //how to get direct from commandline parameter???
  bool isData = false;
  if (fileName.Contains(TRegexp("/ttbar[a-z_A-Z-.0-9]*/"))) //Be careful! the filename is full name!!!!
  {
    std::cout << "Signal matched!" << std::endl;
    m_isSignal = 1;
    m_sample = 1;
  }
  //else if(fileName.Contains(TRegexp("/data[0-9]*/"))){
  else if(fileName.Contains(TRegexp("/data1[0-9]*/"))){
    std::cout << "Data matched!" << std::endl;
    m_isSignal = 0;
    m_sample = -1;
    isData = true;
  }
  else
  {
    std::cout << "Background matched!" << std::endl;
    m_isSignal = 0;
    if (fileName.Contains("/Wenu/") || fileName.Contains("/Wmunu/") || fileName.Contains("/Wtaunu/"))
    {
      m_sample = 2;
    }
    else if (fileName.Contains("/single_top/"))
    {
      m_sample = 3;
    }
    else
    {
      m_sample = 4;
    }
  }

  m_tree = wk()->tree();  
  m_input.initialize(m_tree,isData);
  m_tree->GetEntry(0);
  
  //Do the naming (convention) of the validation histograms
  bool doTruthLabeling = m_config->get<bool>("doTruthLabeling");
  if(m_input.mcChannelNumber==0){
    if (!isData)
    { //two must be compatible
      Warning("changeInput()", "Samples type match failed1!");
      return EL::StatusCode::SUCCESS;
    }
    m_histNameSvc->set_sample("data");
    if(doTruthLabeling) 
      m_histNameSvc->set_truthLabel("notruth");
  }else{
    if (isData)
    {
      Warning("changeInput()", "Samples type match failed2!");
      return EL::StatusCode::SUCCESS;
    }
    //BM: TODO: This is temporary - it would be really nice to have here sth. that gets the name from the mcChannelNumber
    //L: ok lets do it.
    // m_histNameSvc->set_sample(std::to_string(m_input.mcChannelNumber));
     m_histNameSvc->set_sample(getNameFromDSID(m_input.mcChannelNumber));
    if(doTruthLabeling) 
      m_histNameSvc->set_truthLabel("notruth");
  }

  return EL::StatusCode::SUCCESS;
} // changeInput

EL::StatusCode EventSelection::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  Info("EventSelection::initialize()", "Initialize function");
  std::cout << "ConfigStore" << m_config << std::endl;

  if (!m_config)
  {
    Error("setupJob()", "ConfigStore not setup! Remember to set it via setConfig()!");
    //return EL::StatusCode::FAILURE;
  }

  //print config
  //m_config->print();

  //Todo: Use the TopDataPreparation package to do this
  //cross section data base loading
  std::string xsecs_file_path = "${WorkDir_DIR}/data/EventSelection/xsecs/";
  m_config->getif<std::string>("xsecs_file_path", xsecs_file_path);
  CrossSectionDatabase::instance().Load(xsecs_file_path);

  //set event counter to zero
  m_eventCounter = 0;

  //get sum of weights
  Info("EventSelection::initialize()", "Initialize sum of weights provider");
  std::string sumOfWeightsFile = m_config->get<std::string>("sumOfWeightsFile");
  m_sumOfWeightsProvider = new sumOfWeightsProvider(sumOfWeightsFile);
   
  return EL::StatusCode::SUCCESS;
} // initialize

EL::StatusCode EventSelection::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  // clear event in object readers
  // for (ObjectReaderBase *reader : m_objectReader) {
  //   reader->clearEvent();
  // }

  return EL::StatusCode::SUCCESS;
} // postExecute

EL::StatusCode EventSelection::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  // Info("finalize()", "Finalizing job.");

  // EL_CHECK("finalize", finalizeTools());

  // if (m_eventSelection) {
  //   CutFlowCounter counter = m_eventSelection->getCutFlowCounter();
  //   TH1D* cutflowHisto_preselection = counter.getCutFlow("CutFlow/");
  //   wk()->addOutput(cutflowHisto_preselection);
  // }

  // Info("finalize()", "Processed events         = %li", m_eventCounter);
  // Info("finalize()", "Passed nominal selection = %li", m_eventCountPassed);

  return EL::StatusCode::SUCCESS;
} // finalize

EL::StatusCode EventSelection::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  Info("EventSelection::histFinalize()", "Finalizing histograms.");
  m_histSvc->Write(wk());

  return EL::StatusCode::SUCCESS;
} // histFinalize

///////////////////////////////////////////////////////////////////////
// These functions are dependent on the structure of the input tree ///
///////////////////////////////////////////////////////////////////////
bool EventSelection::passTrigger()
{
  int year = collectionYear(); //because the used triggers may change from year to year
  int n_elTrigger = 0;
  int n_muTrigger = 0;
  //relevant unprescaled trigger list taken from https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LowestUnprescaled
  if (year == 2015)
  {
    //electron triggers
    //if (int(m_input.HLT_e24_lhmedium_L1EM20VH) == 1)
    //{
    //  n_elTrigger++;
    //}
    if (int(m_input.HLT_e60_lhmedium_nod0) == 1)
    {
      n_elTrigger++;
    }
    //if (int(m_input.HLT_e120_lhloose) == 1)
    //{
    //  n_elTrigger++;
    //}
    //muon triggers
    //if (int(m_input.HLT_mu20_iloose_L1MU15) == 1)
    //{
    //  n_muTrigger++;
    //}
    if (int(m_input.HLT_mu50) == 1)
    {
      n_muTrigger++;
    }
  }
  else if (year == 2016)
  {
    //e
    if (int(m_input.HLT_e26_lhtight_nod0_ivarloose) == 1)
    {
      n_elTrigger++;
    }
    if (int(m_input.HLT_e60_lhmedium_nod0) == 1)
    {
      n_elTrigger++;
    }
    if (int(m_input.HLT_e140_lhloose_nod0) == 1)
    {
      n_elTrigger++;
    }
    // if(int(m_input.HLT_e300_etcut)==1){n_elTrigger++;}
    //mu
    if (int(m_input.HLT_mu26_ivarmedium) == 1)
    {
      n_muTrigger++;
    }
    if (int(m_input.HLT_mu50) == 1)
    {
      n_muTrigger++;
    }
  }
  else if (year == 2017)
  {
    //e
    if (int(m_input.HLT_e26_lhtight_nod0_ivarloose) == 1)
    {
      n_elTrigger++;
    }
    if (int(m_input.HLT_e60_lhmedium_nod0) == 1)
    {
      n_elTrigger++;
    }
    if (int(m_input.HLT_e140_lhloose_nod0) == 1)
    {
      n_elTrigger++;
    }
    // if(int(m_input.HLT_e300_etcut)==1){n_elTrigger++; }
    //mu
    if (int(m_input.HLT_mu26_ivarmedium) == 1)
    {
      n_muTrigger++;
    }
    if (int(m_input.HLT_mu50) == 1)
    {
      n_muTrigger++;
    }
  }
  else if (year == 2018)
  {
    //e
    if (int(m_input.HLT_e26_lhtight_nod0_ivarloose) == 1)
    {
      n_elTrigger++;
    }
    if (int(m_input.HLT_e60_lhmedium_nod0) == 1)
    {
      n_elTrigger++;
    }
    if (int(m_input.HLT_e140_lhloose_nod0) == 1)
    {
      n_elTrigger++;
    }
    // if(int(m_input.HLT_e300_etcut)==1){n_elTrigger++; }
    //mu
    if (int(m_input.HLT_mu26_ivarmedium) == 1)
    {
      n_muTrigger++;
    }
    if (int(m_input.HLT_mu50) == 1)
    {
      n_muTrigger++;
    }
  }
  if (n_muTrigger > 0 || n_elTrigger > 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//check whether the trigger matched lepton is also the one that is stored in the container (should be true in most cases, but better to check anyway)
int EventSelection::checkElectronMatch()
{
  int nEl = (int)m_input.el_pt->size();
  int year = collectionYear();
  int matches = 0;
  for (int i = 0; i < nEl; i++)
  {
    if (year == 2015)
    {
      if ((int(m_input.el_trigMatch_HLT_e24_lhmedium_L1EM20VH->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e60_lhmedium->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e120_lhloose->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e60_lhmedium_nod0->at(i)) == 1)
          )
      {
        matches++;
      }
    }
    else if (year == 2016)
    {
      if ((int(m_input.el_trigMatch_HLT_e26_lhtight_nod0_ivarloose->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e60_lhmedium_nod0->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e140_lhloose_nod0->at(i)) == 1)
            || (int(m_input.el_trigMatch_HLT_e300_etcut->at(i)) == 1)
      )
      {
        matches++;
      }
    }
    else if (year == 2017)
    {
      if ((int(m_input.el_trigMatch_HLT_e26_lhtight_nod0_ivarloose->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e60_lhmedium_nod0->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e140_lhloose_nod0->at(i)) == 1)
            || (int(m_input.el_trigMatch_HLT_e300_etcut->at(i)) == 1)
      )
      {
        matches++;
      }
    }
    else if (year == 2018)
    {
      if ((int(m_input.el_trigMatch_HLT_e26_lhtight_nod0_ivarloose->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e60_lhmedium_nod0->at(i)) == 1) ||
          (int(m_input.el_trigMatch_HLT_e140_lhloose_nod0->at(i)) == 1)
           ||  (int(m_input.el_trigMatch_HLT_e300_etcut->at(i)) == 1)
      )
      {
        matches++;
      }
    }
  }
  return matches;
}

int EventSelection::checkMuonMatch()
{
  int nMu = (int)m_input.mu_pt->size();
  int year = collectionYear();
  int matches = 0;
  for (int i = 0; i < nMu; i++)
  {
    if (year == 2015)
    {
      //if ((int(m_input.mu_trigMatch_HLT_mu20_iloose_L1MU15->at(i)) == 1) ||
      if (
          (int(m_input.mu_trigMatch_HLT_mu50->at(i)) == 1))
      {
        matches++;
      }
    }
    else if (year == 2016)
    {
      if ((int(m_input.mu_trigMatch_HLT_mu26_ivarmedium->at(i)) == 1) ||
          (int(m_input.mu_trigMatch_HLT_mu50->at(i)) == 1))
      {
        matches++;
      }
    }
    else if (year == 2017)
    {
      if ((int(m_input.mu_trigMatch_HLT_mu26_ivarmedium->at(i)) == 1) ||
          (int(m_input.mu_trigMatch_HLT_mu50->at(i)) == 1))
      {
        matches++;
      }
    }
    else if (year == 2018)
    {
      if ((int(m_input.mu_trigMatch_HLT_mu26_ivarmedium->at(i)) == 1) ||
          (int(m_input.mu_trigMatch_HLT_mu50->at(i)) == 1))
      {
        matches++;
      }
    }
  }
  return matches;
}

int EventSelection::collectionYear()
{
  //if MC, use random run number
  int run = (m_input.mcChannelNumber == 0) ? m_input.runNumber : m_input.randomRunNumber;
  //match the run numbers with the corresponding years (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2)
  if ((run >= 276262) && (run <= 284484))
  { //numbers obtained from the used GRLs
    return 2015;
  }
  else if ((run >= 297330) && (run <= 311481))
  {
    return 2016;
  }
  else if ((run >= 325713) && (run <= 340453))
  {
    return 2017;
  }
  else if ((run >= 348885) && (run <= 364292))
  {
    return 2018;
  }
  else
  {
    Warning("EventSelection::CollectionYear()", "The used run number %d corresponds to no known data taking period", run);
    return -1;
  }
}

bool EventSelection::getLepton()
{
  //electron matched event
  if (m_elTrigMatch > 0 && m_muTrigMatch == 0)
  {
    m_lepton.SetPtEtaPhiE(m_input.el_pt->at(0), m_input.el_eta->at(0), m_input.el_phi->at(0), m_input.el_e->at(0));
    return true;
  }
  //muon matched event
  if (m_muTrigMatch > 0 && m_elTrigMatch == 0)
  {
    m_lepton.SetPtEtaPhiE(m_input.mu_pt->at(0), m_input.mu_eta->at(0), m_input.mu_phi->at(0), m_input.mu_e->at(0));
    return true;
  }
  return false;
}

bool EventSelection::getMET()
{
  m_met.SetPtEtaPhiE(m_input.met_met, 0, m_input.met_phi, m_input.met_met);
  return true;
}

bool EventSelection::getJets()
{

  //clear and resize jet container from previously processed event
  m_jets.clear();
  unsigned int nJets = m_input.jet_pt->size();
  m_jets.resize(nJets);

  for (size_t i = 0; i < nJets; ++i)
  {
    m_jets[i].SetPtEtaPhiE(m_input.jet_pt->at(i), m_input.jet_eta->at(i), m_input.jet_phi->at(i), m_input.jet_e->at(i));
  }
  return true;
}
bool EventSelection::getLargeRJets()
{

  //clear and resize jet container from previously processed event
  m_ljets.clear();
  unsigned int nLJets = m_input.ljet_pt->size();
  m_ljets.resize(nLJets);
  if (nLJets > 0)
  {
    for (size_t i = 0; i < nLJets; ++i)
    {
      m_ljets[i].SetPtEtaPhiM(m_input.ljet_pt->at(i), m_input.ljet_eta->at(i), m_input.ljet_phi->at(i), m_input.ljet_m->at(i));
    }
  }
  return true;
}

bool EventSelection::getVRJets()
{

  //clear and resize jet container from previously processed event
  m_VRjets.clear();
  unsigned int nVRJets = m_input.tjet_pt->size();
  m_VRjets.resize(nVRJets);

  for (size_t i = 0; i < nVRJets; ++i)
  {
    m_VRjets[i].SetPtEtaPhiE(m_input.tjet_pt->at(i), m_input.tjet_eta->at(i), m_input.tjet_phi->at(i), m_input.tjet_e->at(i));
  }
 return true;
}
//HERENINI
bool EventSelection::getAssociatedVR()
{
  m_AssociatedVR.clear();
  weight_perAssociatedVR_DL1r85->clear();
  isVHbbTagged_perAssociatedVR->clear();
  Atjet_DL1r->clear();
  
  //unsigned int nAssociatedVR = m_input.ljet1_ghostVR_trkjet_index->size();
  //unsigned int nAssociatedVR = m_input.ljet_ghostVR_indices[m_jetIndex_boosted_top].size();
  unsigned int nAssociatedVR = m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).size();

  m_AssociatedVR.resize(nAssociatedVR);
  Atjet_DL1r->resize(nAssociatedVR);
  weight_perAssociatedVR_DL1r85->resize(nAssociatedVR);
  isVHbbTagged_perAssociatedVR->resize(nAssociatedVR);

  for (size_t i = 0; i < nAssociatedVR; ++i)
  {
    //unsigned int VR_index = m_input.ljet1_ghostVR_trkjet_index->at(i);
    unsigned long VR_index = m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).at(i);
    //unsigned long VR_index = *P_VR_index;
    Atjet_DL1r->at(i) = m_input.tjet_DL1r->at(VR_index);
    m_AssociatedVR[i].SetPtEtaPhiE(m_input.tjet_pt->at(VR_index),m_input.tjet_eta->at(VR_index), m_input.tjet_phi->at(VR_index), m_input.tjet_e->at(VR_index));
    //weight_perAssociatedVR_DL1r85[i] = weight_perjet_trackjet_bTagSF_DL1r_85->at(VR_index);
    //isVHbbTagged_perAssociatedVR[i] = tjet_isbtagged_DL1r_85->at(VR_index);
    if (m_input.mcChannelNumber != 0) weight_perAssociatedVR_DL1r85->at(i) = m_input.weight_perjet_trackjet_bTagSF_DL1r_85->at(VR_index);
    else weight_perAssociatedVR_DL1r85->at(i) = 1.0;
    isVHbbTagged_perAssociatedVR->at(i) = m_input.tjet_isbtagged_DL1r_85->at(VR_index);

  }
 return true;
}

std::string EventSelection::getNameFromDSID(int dsid)
{
  //This is vital, must doublecheck!
  #pragma message("Load hard-coded dsid category, pls check by your self in EventSelection.h...")
  std::set<int> ttbar{410470};
  std::set<int> data{0};
  std::set<int> diboson{363356, 363358, 363359, 363360, 363489, 363355, 363357};
  std::set<int> sitop{410658, 410659, 410646, 410647, 410644, 410645};
  std::set<int> unknown{-1};
  //v+jets
  std::set<int> vjets{};
  for (int i = 364100; i <= 364113; i++)  vjets.insert(i);      //Zmumu
  for (int i = 364114; i <= 364127; i++)  vjets.insert(i);      //Zee
  for (int i = 364128; i <= 364141; i++)  vjets.insert(i);      //Ztautau
  for (int i = 364156; i <= 364169; i++)  vjets.insert(i);      //Wmunu
  for (int i = 364170; i <= 364183; i++)  vjets.insert(i);      //Wenu
  for (int i = 364184; i <= 364197; i++)  vjets.insert(i);      //Wtaunu
  //return
  std::map<std::string, std::set<int>> dsidmap{
      {"ttbar", ttbar},
      {"data", data},
      {"diboson", diboson},
      {"sitop", sitop},
      {"unknown", unknown},
      {"vjets", vjets}
  };
  std::map<std::string, std::set<int>>::iterator iter;
  for (iter = dsidmap.begin(); iter != dsidmap.end(); iter++)
    if (iter->second.find(dsid) != iter->second.end())
      return iter->first;
  return "unknown";
}

std::string EventSelection::getTruthLabel(int* truths, int nVR){
  //b,c,l,t,other
  //currently only consider bb, bc, bl, cc, 
  return "unknown_truth";
}

bool EventSelection::passBoostedPreselection()
{
  //<custom debug flag>
  bool Ldebug = false;
  if (m_config->get<int>("maxEvents")>0){ //avoid debug in -1 runall mode
    m_config->getif<bool>("Ldebug",Ldebug); //in case this is missing
  }
  //</custom debug flag>
  

  //at least 1 large R jet
  if (m_ljets.size() < 1)
  {
    return false;
  }

  fillCutflow(4);

  //met > 70 GeV
  if (m_met.Pt() < 70000)
  {
    return false;
  }

  fillCutflow(5);

  //lepton pT cut > 70 geV
  if (m_lepton.Pt() < 70000)
  {
    return false;
  }

  fillCutflow(6);

  //getting the  highest-pT large-R jet not overlaping with the electron
  m_jetIndex_boosted_top = getBoostedTopJet();
  // m_VRjetIndex_boosted_b_had = getBoostedVRHadJet();
  if(Ldebug){
    std::cout << "get m_jetIndex_boosted_top=" << m_jetIndex_boosted_top << std::endl;
    // std::cout << "get m_VRjetIndex_boosted_b_had=" << m_VRjetIndex_boosted_b_had << std::endl;
  }

  //LjetPt > 300 GeV (already applied in ntuples production.)
  if (m_ljets.at(m_jetIndex_boosted_top).Pt() < 300000)
  {
    return false;
  }

//temporary solution: drop the non-leading fat-jet? how to reweight?

  fillCutflow(7);

  m_jetIndex_boosted_b_lep = getBoostedLepJet();
  // m_VRjetIndex_boosted_b_lep = getBoostedVRLepJet();
  if(Ldebug){
    std::cout << ">> m_jetIndex_boosted_b_lep=" << m_jetIndex_boosted_b_lep << std::endl;
    // std::cout << ">> m_VRetIndex_boosted_b_lep=" << m_VRjetIndex_boosted_b_lep << std::endl;
  }

  //|Dphi|>1 
  // NOTE LV: this is not exactly what was done before, but we can see if this works better
  //          I was using the deltaPhi between the two top candidates. ?? how you do this?
  //L: Can we directly use dR_lepton_fatjet??
  if (fabs(m_ljets.at(m_jetIndex_boosted_top).DeltaPhi(m_jets.at(m_jetIndex_boosted_b_lep))) < 1)
  {
    return false;
  }

  fillCutflow(8);

  double ljetH_m = 50000;
  m_config->getif<double>("ljetH_m",ljetH_m);//in case this is missing
  //Ljet mass > 100 GeV //from here, events be cutted.
  //adjust to 50GeV
  if (m_ljets.at(m_jetIndex_boosted_top).M() < ljetH_m)
  {
    return false;
  }

  fillCutflow(9);

//the matched small-R jet in leptonic side is b-tagged.
//Or can we use VR?
//Or can we use exist 1 btagged outside large-R??
  if (!m_jets_isTagged.at(m_jetIndex_boosted_b_lep))
  {
    return false;
  }

  fillCutflow(10);

  //add non-leading large-R jet cut
  if(m_jetIndex_boosted_top!=0){
    return false;
  }
  fillCutflow(11);

  //NINI at least 2 ghost-associated VR track jets
//HAHAHA
  getAssociatedVR();
  if(m_AssociatedVR.size()<2){
    return false;
  }
  fillCutflow(12);

  //if all cuts passed, return true
  return true;
}

int EventSelection::getBoostedLepJet()
{
  // return the index of the jet closest to the lepton
  double dRmin = -1.0;
  int jet_index = -1;
  for (size_t i = 0; i < m_jets.size(); ++i)
  {
    const double thisDr = m_jets[i].DeltaR(m_lepton);
    if (thisDr < dRmin || dRmin < 0)
    {
      dRmin = thisDr;
      jet_index = i;
    }
  }
  return jet_index;
}

int EventSelection::getBoostedTopJet() //must pT sorted!!!!
{
  //In the electron channel, it happens that the semi-leptonically-decaying top
  //quark is reconstructed as a large-R jet (in cases where the large-R jet and
  //the electron are close). Trying to remove such large-R jets assuming they
  //pass the pT and mass cuts required for top tagging.
  if (m_elTrigMatch > 0 && m_muTrigMatch == 0)
  {
    if (m_ljets.size() == 1)
      return 0;
    for (size_t i = 0; i < m_ljets.size(); ++i)
    {
      const double thisDr = m_ljets[i].DeltaR(m_lepton);
      if (thisDr > 1.)
      {
        return i;
      }
    }
  }
  else
  {
    return 0;
  }
  return 0;
}

double EventSelection::getMwt()
{
  //definition of transverse W mass (invariant mass with only transverse components)
  return sqrt(2. * m_lepton.Pt() * m_met.Pt() * (1. - cos(m_lepton.DeltaPhi(m_met))));
}

double EventSelection::getEventWeight()
{

  //check if data
  double mcChannel = m_input.mcChannelNumber;
  if (mcChannel == 0)
  {
    return 1.0;
  }

  //else calculate the event weight
  double weight(-1.0);
  double sumOfWeights = m_sumOfWeightsProvider->getsumOfWeights(m_input.mcChannelNumber);
  double lumi = m_config->get<double>("lumi");
  double mc_weight = m_input.weight_mc;


  //Sherpa large weight protection (see e.g. https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PmgWeakBosonProcesses#Treating_pathological_high_weigh)
  // Sherpa 221 have large weight eventsfor debug. Just set weight=1. Or use sherpa>=2.2.4
  if (fabs(mc_weight) > 100)
  {
    if (
        (mcChannel >= 363355 && mcChannel <= 363360) || //Sherpa Diboson
        (mcChannel == 363489) ||                        //Sherpa WlvZqq
        (mcChannel >= 364100 && mcChannel <= 364197) || //Sherpa V+jets
        (mcChannel >= 364216 && mcChannel <= 364229)    //Sherpa V+jets HPT slices
    )
    {
      mc_weight = 1.0;
      Warning("EventSelection::getEventWeight()", "Sherpa 2.2.1 debug large weight => set to 1");
    } //if fabs weight
  }

//NINI
  float btagWeight = m_input.weight_trackjet_bTagSF_DL1r_85;
  //if(m_AssociatedVR.size()==2) btagWeight = weight_perAssociatedVR_DL1r85->at(0) * weight_perAssociatedVR_DL1r85->at(1);
  //if(m_AssociatedVR.size()>2) btagWeight = weight_perAssociatedVR_DL1r85->at(0) * weight_perAssociatedVR_DL1r85->at(1) * weight_perAssociatedVR_DL1r85->at(2);
  weight = lumi * mc_weight * m_input.weight_pileup * m_input.weight_leptonSF * btagWeight * m_input.weight_jvt * CrossSectionDatabase::instance().GetEffectiveXs(m_input.mcChannelNumber) / sumOfWeights;
  //weight = lumi * mc_weight * m_input.weight_pileup * m_input.weight_leptonSF * m_input.weight_jvt * CrossSectionDatabase::instance().GetEffectiveXs(m_input.mcChannelNumber) / sumOfWeights;
  //weight = btagWeight;

  //to test for fishy negative weights
  if(weight < -10000){
    // std::cout << "lumi: " << lumi << ", mc: " << m_input.weight_mc << ", pu: " << m_input.weight_pileup << ", lepSF: " << m_input.weight_leptonSF << ", jvt:  " << m_input.weight_jvt << ", xs: " << CrossSectionDatabase::instance().GetEffectiveXs(m_input.mcChannelNumber) << ", sow: " << sumOfWeights << std::endl;
    Warning("EventSelection::getEventWeight()", "Large Negative weight.");
  }

  if (weight == weight)
  { //exclude nan values
    return weight;
  }
  Warning("EventSelection::getEventWeight()", "NaN weight values");
  return -1.0;
}

EL::StatusCode EventSelection::fillHistograms()
{
    //<custom debug flag>
  bool Ldebug = false;
  if (m_config->get<int>("maxEvents")>0){ //avoid debug in -1 runall mode
    m_config->getif<bool>("Ldebug",Ldebug); //in case this is missing
  }

  m_histSvc->BookFillHist("lepPt", 20, 0, 300, m_lepton.Pt() / 1.0e3, m_eventWeight);
  m_histSvc->BookFillHist("lepEta", 20, -2.5, 2.5, m_lepton.Eta(), m_eventWeight);
  m_histSvc->BookFillHist("lepPhi", 35, -3.5, 3.5, m_lepton.Phi(), m_eventWeight);

  m_histSvc->BookFillHist("jet1Pt", 40, 0, 600, m_jets.at(0).Pt() / 1.0e3, m_eventWeight);
  m_histSvc->BookFillHist("jet1Eta", 20, -2.5, 2.5, m_jets.at(0).Eta(), m_eventWeight);
  m_histSvc->BookFillHist("jet1Phi", 35, -3.5, 3.5, m_jets.at(0).Phi(), m_eventWeight);

if (m_jets.size() > 1){
  m_histSvc->BookFillHist("jet2Pt", 40, 0, 600, m_jets.at(1).Pt() / 1.0e3, m_eventWeight);
  m_histSvc->BookFillHist("jet2Eta", 20, -2.5, 2.5, m_jets.at(1).Eta(), m_eventWeight);
  m_histSvc->BookFillHist("jet2Phi", 35, -3.5, 3.5, m_jets.at(1).Phi(), m_eventWeight);

  if (m_jets.size() > 2)
  {
    m_histSvc->BookFillHist("jet3Pt", 40, 0, 600, m_jets.at(2).Pt() / 1.0e3, m_eventWeight);
    m_histSvc->BookFillHist("jet3Eta", 20, -2.5, 2.5, m_jets.at(2).Eta(), m_eventWeight);
    m_histSvc->BookFillHist("jet3Phi", 35, -3.5, 3.5, m_jets.at(2).Phi(), m_eventWeight);
    if (m_jets.size() > 3)
    {
      m_histSvc->BookFillHist("jet4Pt", 40, 0, 600, m_jets.at(3).Pt() / 1.0e3, m_eventWeight);
      m_histSvc->BookFillHist("jet4Eta", 20, -2.5, 2.5, m_jets.at(3).Eta(), m_eventWeight);
      m_histSvc->BookFillHist("jet4Phi", 35, -3.5, 3.5, m_jets.at(3).Phi(), m_eventWeight);
    }
  }
}
  m_histSvc->BookFillHist("met", 40, 0, 450, m_met.Pt() / 1.0e3, m_eventWeight);
  m_histSvc->BookFillHist("met_phi", 35, -3.5, 3.5, m_met.Phi(), m_eventWeight);

  m_histSvc->BookFillHist("mtw", 80, 0, 400, getMwt() / 1.0e3, m_eventWeight);

  m_histSvc->BookFillHist("njet", 14, 0, 14, m_jets.size(), m_eventWeight);

  if (m_ljets.size() > 0)
  {
    m_histSvc->BookFillHist("Ljet1Pt", 40, 250, 1200, m_ljets.at(0).Pt() / 1.0e3, m_eventWeight); //leading large-R jet
    m_histSvc->BookFillHist("Ljet1Eta", 20, -2.5, 2.5, m_ljets.at(0).Eta(), m_eventWeight);
    m_histSvc->BookFillHist("Ljet1Phi", 35, -3.5, 3.5, m_ljets.at(0).Phi(), m_eventWeight);
    m_histSvc->BookFillHist("Ljet1M", 60, 0, 300, m_ljets.at(0).M() / 1.0e3, m_eventWeight);
    //    m_histSvc->BookFillHist("DPhi", 35, -3.5, 3.5, m_ljets.at(0).Phi() - m_chi2.topLphi, m_eventWeight);
  }
  m_histSvc->BookFillHist("nLjet", 14, 0, 14, m_ljets.size(), m_eventWeight);

  int nb = 0;
  for (auto t : m_jets_isTagged)
  {
    if (t)
      nb++;
  }
  m_histSvc->BookFillHist("nbjet", 4, 0, 4, nb, m_eventWeight);

  int probe_jet_index = -1;
  int tag_jet_index = -1;

    tag_jet_index = m_jetIndex_boosted_b_lep;
    probe_jet_index = m_jetIndex_boosted_top;

  const auto &pj = m_ljets.at(probe_jet_index);
  // store probe jet histograms
    m_histSvc->BookFillHist("probe_pt", 40, 250, 1200, pj.Pt() / 1.0e3, m_eventWeight); //matched large-R jet
    m_histSvc->BookFillHist("probe_eta", 20, -2.5, 2.5, pj.Eta(), m_eventWeight);
    m_histSvc->BookFillHist("probe_phi", 35, -3.5, 3.5, pj.Phi(), m_eventWeight);
    m_histSvc->BookFillHist("probe_m", 60, 0, 300, pj.M() / 1.0e3, m_eventWeight);
 
  if (tag_jet_index > -1)
  {
    const auto &tj = m_jets.at(tag_jet_index);
    m_histSvc->BookFillHist("tag_pt", 40, 200, 600, tj.Pt() / 1.0e3, m_eventWeight);
    m_histSvc->BookFillHist("tag_eta", 20, -2.5, 2.5, tj.Eta(), m_eventWeight);
    m_histSvc->BookFillHist("tag_phi", 35, -3.5, 3.5, tj.Phi(), m_eventWeight);
    m_histSvc->BookFillHist("tag_m", 50, 50, 300, tj.M() / 1.0e3, m_eventWeight);
    m_histSvc->BookFillHist("tag_mlj", 60, 0, 300, get_mlj() / 1.0e3, m_eventWeight);
  }
  //  m_histSvc->BookFillHist("probe_drmin", 40, 0, 4, getJetDrMin(m_jetIndex_b_had), m_eventWeight);
  //  m_histSvc->BookFillHist("probe_dRVJ", 40, 0, 0.5, m_input.jet_deltaR->at(m_jetIndex_b_had), m_eventWeight);

double DH=m_input.ljet_Xbb202006_Higgs->at(m_jetIndex_boosted_top);
double DQ=m_input.ljet_Xbb202006_QCD->at(m_jetIndex_boosted_top);
double DT=m_input.ljet_Xbb202006_Top->at(m_jetIndex_boosted_top);
double DHv3=m_input.ljet_m_Xbb2020v3_Higgs->at(m_jetIndex_boosted_top);
double DQv3=m_input.ljet_m_Xbb2020v3_QCD->at(m_jetIndex_boosted_top);
double DTv3=m_input.ljet_m_Xbb2020v3_Top->at(m_jetIndex_boosted_top);

m_histSvc->BookFillHist("DXbb_Higgs", 100, 0, 1, DH, m_eventWeight);
m_histSvc->BookFillHist("DXbb_Top", 100, 0, 1, DT, m_eventWeight);
m_histSvc->BookFillHist("DXbb_QCD", 100, 0, 1, DQ, m_eventWeight);
m_histSvc->BookFillHist("DXbb_Higgs_v3", 100, 0, 1, DHv3, m_eventWeight);
m_histSvc->BookFillHist("DXbb_Top_v3", 100, 0, 1, DTv3, m_eventWeight);
m_histSvc->BookFillHist("DXbb_QCD_v3", 100, 0, 1, DQv3, m_eventWeight);

// m_histSvc->BookFillHist("ljet_pt", 200, 0, 2000, m_ljets.at(m_jetIndex_boosted_top).Pt() / 1.0e3, m_eventWeight); //moved to probe
// m_histSvc->BookFillHist("ljet_m", 200, 0, 1000, m_ljets.at(m_jetIndex_boosted_top).M() / 1.0e3, m_eventWeight);
// m_histSvc->BookFillHist("ljet_pt_fine", 120, 0, 1200, m_ljets.at(m_jetIndex_boosted_top).Pt() / 1.0e3, m_eventWeight);
// m_histSvc->BookFillHist("ljet_m_fine", 80, 0, 400, m_ljets.at(m_jetIndex_boosted_top).M() / 1.0e3, m_eventWeight);


m_histSvc->BookFillHist("DXbb_t0", 200, -10, 10, log2(DH/DQ), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t1", 200, -10, 10, log2(DH/DT), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t0p5", 200, -10, 10, log2(DH/(0.5*DT+0.5*DQ)), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t0p25" ,200, -10, 10, log2(DH/(0.25*DT+0.75*DQ)), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t0_v3", 200, -10, 10, log2(DHv3/DQv3), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t1_v3", 200, -10, 10, log2(DHv3/DTv3), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t0p5_v3", 200, -10, 10, log2(DHv3/(0.5*DTv3+0.5*DQv3)), m_eventWeight);
m_histSvc->BookFillHist("DXbb_t0p25_v3" ,200, -10, 10, log2(DHv3/(0.25*DTv3+0.75*DQv3)), m_eventWeight);

//m_histSvc->BookFillHist("n_ljet1_VRjet", 8, 0, 8, m_input.ljet1_ghostVR_trkjet_raw_index->size(), m_eventWeight);
//m_histSvc->BookFillHist("n_ljet1_VRjet", 8, 0, 8, m_input.ljet_ghostVR_indices[m_jetIndex_boosted_top].size(), m_eventWeight);
m_histSvc->BookFillHist("n_ljet1_VRjet", 8, 0, 8, m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).size(), m_eventWeight);

//NINI
float DL1rTrk1=-99.;
float DL1rTrk2=-99.;
float DL1rTrk3=-99.;
float ptTrk1=-99.;
float ptTrk2=-99.;
float ptTrk3=-99.;
float etaTrk1=-99.;
float etaTrk2=-99.;
float etaTrk3=-99.;
float phiTrk1=-99.;
float phiTrk2=-99.;
float phiTrk3=-99.;
float mTrk1=-99.;
float mTrk2=-99.;
float mTrk3=-99.;

if(m_AssociatedVR.size()>0){
  DL1rTrk1 = Atjet_DL1r->at(0);
  ptTrk1 = m_AssociatedVR.at(0).Pt() / 1.0e3;
  etaTrk1 = m_AssociatedVR.at(0).Eta();
  phiTrk1 = m_AssociatedVR.at(0).Phi();
//  mTrk1 = m_input.ljet1_ghostVR_trkjet_m->at(0) / 1.0e3;
  if(m_AssociatedVR.size()>1){
    DL1rTrk2 = Atjet_DL1r->at(1);
    ptTrk2 = m_AssociatedVR.at(1).Pt() / 1.0e3;
    etaTrk2 = m_AssociatedVR.at(1).Eta();
    phiTrk2 = m_AssociatedVR.at(1).Phi();
//    mTrk2 = m_input.ljet1_ghostVR_trkjet_m->at(1) / 1.0e3;
    if(m_AssociatedVR.size()>2){
      DL1rTrk3 = Atjet_DL1r->at(2);
      ptTrk3 = m_AssociatedVR.at(2).Pt() / 1.0e3;
      etaTrk3 = m_AssociatedVR.at(2).Eta();
      phiTrk3 = m_AssociatedVR.at(2).Phi();
//      mTrk3 = m_input.ljet1_ghostVR_trkjet_m->at(2) / 1.0e3;
    }
  }
}
 
m_histSvc->BookFillHist("DL1rTrk1", 20, -1, 1, DL1rTrk1, m_eventWeight);
m_histSvc->BookFillHist("DL1rTrk2", 20, -1, 1, DL1rTrk2, m_eventWeight);
m_histSvc->BookFillHist("DL1rTrk3", 20, -1, 1, DL1rTrk3, m_eventWeight);

m_histSvc->BookFillHist("mTrk1", 20, 0, 500, mTrk1, m_eventWeight);
m_histSvc->BookFillHist("mTrk2", 20, 0, 500, mTrk2, m_eventWeight);
m_histSvc->BookFillHist("mTrk3", 20, 0, 500, mTrk3, m_eventWeight);

m_histSvc->BookFillHist("ptTrk1", 20, 0, 1200, ptTrk1, m_eventWeight);
m_histSvc->BookFillHist("ptTrk2", 20, 0, 1200, ptTrk2, m_eventWeight);
m_histSvc->BookFillHist("ptTrk3", 20, 0, 1200, ptTrk3, m_eventWeight);

m_histSvc->BookFillHist("etaTrk1", 20, -3, 3, etaTrk1, m_eventWeight);
m_histSvc->BookFillHist("etaTrk2", 20, -3, 3, etaTrk2, m_eventWeight);
m_histSvc->BookFillHist("etaTrk3", 20, -3, 3, etaTrk3, m_eventWeight);

m_histSvc->BookFillHist("phiTrk1", 20, 0, 5, phiTrk1, m_eventWeight);
m_histSvc->BookFillHist("phiTrk2", 20, 0, 5, phiTrk2, m_eventWeight);
m_histSvc->BookFillHist("phiTrk3", 20, 0, 5, phiTrk3, m_eventWeight);

int isVHbbTagged=0;
if(m_AssociatedVR.size()==2) isVHbbTagged=isVHbbTagged_perAssociatedVR->at(0)+isVHbbTagged_perAssociatedVR->at(1);
else if(m_AssociatedVR.size()>2) isVHbbTagged=isVHbbTagged_perAssociatedVR->at(0)+isVHbbTagged_perAssociatedVR->at(1)+isVHbbTagged_perAssociatedVR->at(2);
m_histSvc->BookFillHist("isVHbbTagged", 5, 0, 5, isVHbbTagged, m_eventWeight);

return EL::StatusCode::SUCCESS;
}

double EventSelection::getJetDrMin(size_t ref)
{
  //find mindR inside jets collection
  assert(ref < m_jets.size());

  double dRmin = -1.0;
  const auto &refP4 = m_jets.at(ref);

  for (size_t i = 0; i < m_jets.size(); ++i)
  {
    if (i == ref)
    {
      continue;
    }

    const double thisDr = m_jets[i].DeltaR(refP4);
    if (thisDr < dRmin || dRmin < 0)
    {
      dRmin = thisDr;
    }
  }
  return dRmin;
}

void EventSelection::fillCutflow(int stage)
{
  int year = collectionYear();
  string sample = m_histNameSvc->get_sample();
  if (strcmp(sample.c_str(), "data") == 0)
  {
    if (year == 2015)
    {
      if (m_elTrigMatch > 0)
        m_cf_15_el_dat->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_15_mu_dat->Fill(stage, m_eventWeight);
    }
    else if (year == 2016)
    {
      if (m_elTrigMatch > 0)
        m_cf_16_el_dat->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_16_mu_dat->Fill(stage, m_eventWeight);
    }
    else if (year == 2017)
    {
      if (m_elTrigMatch > 0)
        m_cf_17_el_dat->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_17_mu_dat->Fill(stage, m_eventWeight);
    }
    else if (year == 2018)
    {
      if (m_elTrigMatch > 0)
        m_cf_18_el_dat->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_18_mu_dat->Fill(stage, m_eventWeight);
    }
  }
  else
  {
    if (year == 2015)
    {
      if (m_elTrigMatch > 0)
        m_cf_15_el_mc->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_15_mu_mc->Fill(stage, m_eventWeight);
    }
    else if (year == 2016)
    {
      if (m_elTrigMatch > 0)
        m_cf_16_el_mc->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_16_mu_mc->Fill(stage, m_eventWeight);
    }
    else if (year == 2017)
    {
      if (m_elTrigMatch > 0)
        m_cf_17_el_mc->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_17_mu_mc->Fill(stage, m_eventWeight);
    }
    else if (year == 2018)
    {
      if (m_elTrigMatch > 0)
        m_cf_18_el_mc->Fill(stage, m_eventWeight);
      if (m_muTrigMatch > 0)
        m_cf_18_mu_mc->Fill(stage, m_eventWeight);
    }
  }

  return;
}

double EventSelection::get_mlj()
{
  if (m_jetIndex_boosted_b_lep > -1 && (unsigned int)m_jetIndex_boosted_b_lep < m_jets.size())
  {
    const auto &pj = m_jets.at(m_jetIndex_boosted_b_lep);
    return (pj + m_lepton).M();
  }
  return -1;
}

double EventSelection::get_dR_lj_min()
{
  //get min dR(lepton) in jets collection
  float dRmin = -1;
  for (size_t i = 0; i < m_jets.size(); ++i)
  {
    float thisDr = m_jets.at(i).DeltaR(m_lepton);
    if (thisDr < dRmin || dRmin < 0)
    {
      dRmin = thisDr;
    }
  }
  return dRmin;
}

double EventSelection::get_Lep_d0()
{
  //get lepton d0
  bool isEl = (m_elTrigMatch > 0);
  bool isMu = (m_muTrigMatch > 0);

  if (isEl)
  {
    return m_input.el_d0sig->at(0);
  }
  else if (isMu)
  {
    return m_input.mu_d0sig->at(0);
  }
  return -99;
}

double EventSelection::get_lep_topoetcone()
{
  bool isEl = (m_elTrigMatch > 0);
  bool isMu = (m_muTrigMatch > 0);

  if (isEl)
  {
    return m_input.el_topoetcone20->at(0);
  }
  else if (isMu)
  {
    return m_input.mu_topoetcone20->at(0);
  }
  return -99;
}

double EventSelection::get_lep_ptvarcone()
{
  bool isEl = (m_elTrigMatch > 0);
  bool isMu = (m_muTrigMatch > 0);

  if (isEl)
  {
    return m_input.el_ptvarcone20->at(0);
  }
  else if (isMu)
  {
    return m_input.mu_ptvarcone30->at(0);
  }
  return -99;
}

//L: not avilible now
/*
double EventSelection::get_lep_new_iso(){
  bool isEl = (m_elTrigMatch > 0);
  bool isMu = (m_muTrigMatch > 0);
  
  if(isEl){
    return m_input.el_ptvarcone20_TightTTVA_pt1000->at(0);
  }else if(isMu){
    return m_input.mu_ptvarcone30_TightTTVA_pt1000->at(0);
  }
  return -99;
}
*/

bool EventSelection::get_isTightLep()
{
  bool isEl = (m_elTrigMatch > 0);
  bool isMu = (m_muTrigMatch > 0);

  if (isEl)
  {
    return bool(m_input.el_isTight->at(0));
  }
  else if (isMu)
  {
    return bool(m_input.mu_isTight->at(0));
  }
  return false;
}

EL::StatusCode EventSelection::fillBoostedProbeJetTree()
{

  m_probeIndex = m_jetIndex_boosted_top; //!

  const auto &pj = m_ljets.at(m_probeIndex);
  const auto &tagj = m_jets.at(m_jetIndex_boosted_b_lep);
  ProbeJetTree *pjt;
  pjt = m_probetree;

  //now check which channel and connect to the correct probe jet tree object
  bool isEl = (m_elTrigMatch > 0);
  bool isMu = (m_muTrigMatch > 0);

  int year = collectionYear();

  if (year == 2015)
  {
    pjt->CollectionYear = 2015;
  }
  else if (year == 2016)
  {
    pjt->CollectionYear = 2016;
  }
  else if (year == 2017)
  {
    pjt->CollectionYear = 2017;
  }
  else if (year == 2018)
  {
    pjt->CollectionYear = 2018;
  }
  if(!pjt)  {
        Error("fillBoostedProbeJetTree()", "Probe Tree not ready.");
    return EL::StatusCode::SUCCESS;
  }

  //fill the variables
  //L: not fill chi2 now
  // pjt->chi2 = log10(m_chi2min_total);
  // pjt->chi2 = -1;
  // pjt->dRmin = getJetDrMin(m_probeIndex); // useless, not care dR between jet
  // pjt->dRmin = -1;
  //L: no jet_deltaR saved...
  // pjt->dRVtxJet = m_input.jet_deltaR->at(m_probeIndex);
  // pjt->dRmin=-1;
  // pjt->dRVtxJet = -1;

  //complex kinet
  pjt->mlj = get_mlj(); //mlj in leptonic side
  pjt->mtw = getMwt();

  //lepton kinet
  if (isEl)
  {
    pjt->lepq = m_input.el_charge->at(0);
    pjt->lep_type = 1;
  }
  else if (isMu)
  {
    pjt->lepq = m_input.mu_charge->at(0);
    pjt->lep_type = 2;
  }
  pjt->lep_pt = m_lepton.Pt();
  pjt->lep_eta = m_lepton.Eta();
  pjt->lep_phi = m_lepton.Phi();

  //met kinet
  pjt->met_met = m_met.Pt();
  pjt->met_phi = m_met.Phi();

  pjt->weight_trk1_btag_60WP = weight_perAssociatedVR_DL1r85->at(0);


  //large-R/probe kniet
  pjt->jetIndex_boosted_top=m_jetIndex_boosted_top;
  pjt->pt = pj.Pt();
  pjt->eta = pj.Eta();
  pjt->phi = pj.Phi();
  pjt->m = pj.M();

  //tagJet/small-R jet kinet
  pjt->jetIndex_boosted_b_lep=m_jetIndex_boosted_b_lep;
  pjt->tagJet_pt = tagj.Pt();
  pjt->tagJet_eta = tagj.Eta();
  pjt->tagJet_phi = tagj.Phi();
  pjt->tagJet_m = tagj.M();
  pjt->tagJet_isTagged77 = m_input.jet_isbtagged_DL1r_77->at(m_jetIndex_boosted_b_lep);
  pjt->tagJet_isTagged85 = m_input.jet_isbtagged_DL1r_85->at(m_jetIndex_boosted_b_lep);
  pjt->tagJet_isTagged70 = m_input.jet_isbtagged_DL1r_70->at(m_jetIndex_boosted_b_lep);
  pjt->tagJet_isTagged60 = m_input.jet_isbtagged_DL1r_85->at(m_jetIndex_boosted_b_lep);

  //jet count
  pjt->njets = m_jets.size();
  pjt->nljets = m_ljets.size();
  pjt->nVRjets = m_input.tjet_pt->size();
  int nb = 0;
  for (auto t : m_jets_isTagged)
  {
    if (t)
      nb++;
  }
  pjt->nbjets = nb;
  pjt->nLep = m_elTrigMatch+m_muTrigMatch;


  // pjt->mtop = m_ljets.at(m_jetIndex_boosted_top).M(); //move to m

  //truth jet label for MC
  //data: 3282
  //L: ???
  //truth label
  bool isData = (m_input.mcChannelNumber == 0);
  if (isData)
  {
    pjt->label = 3282;
    pjt->tagJet_label = -99;
    pjt->lep_true = -99;
  }
  else
  {
    pjt->label = m_input.ljet_truthLabel->at(m_probeIndex);
    pjt->tagJet_label = m_input.jet_truthflav->at(m_jetIndex_boosted_b_lep);
    if (isEl)
    {
      pjt->lep_true = m_input.el_true_type->at(0);
    }
    else if (isMu)
    {
      pjt->lep_true = m_input.mu_true_type->at(0);
    }
  }

  //b-tagger scores
  // pjt->mv2c10 = m_input.jet_mv2c10->at(m_probeIndex); //L: not usefull now., even if we have mv2c10 in input
  // pjt->mv2c10 = -1;

  //fill weights
  pjt->ev_weight = m_eventWeight;
  pjt->xs_weight = m_input.weight_mc;
  pjt->ev_weight_pileup_UP = m_input.weight_pileup_UP; 
  pjt->ev_weight_jvt_UP = m_input.weight_jvt_UP; 
  pjt->ev_weight_leptonSF_EL_SF_Trigger_UP = m_input.weight_leptonSF_EL_SF_Trigger_UP;
  pjt->ev_weight_leptonSF_EL_SF_Reco_UP =  m_input.weight_leptonSF_EL_SF_Reco_UP;
  pjt->ev_weight_leptonSF_EL_SF_ID_UP =  m_input.weight_leptonSF_EL_SF_ID_UP;
  pjt->ev_weight_leptonSF_EL_SF_Isol_UP =  m_input.weight_leptonSF_EL_SF_Isol_UP;
  pjt->ev_weight_leptonSF_MU_SF_Trigger_STAT_UP = m_input.weight_leptonSF_MU_SF_Trigger_STAT_UP;
  pjt->ev_weight_leptonSF_MU_SF_Trigger_SYST_UP = m_input.weight_leptonSF_MU_SF_Trigger_SYST_UP;
  pjt->ev_weight_leptonSF_MU_SF_ID_STAT_UP = m_input.weight_leptonSF_MU_SF_ID_STAT_UP;
  pjt->ev_weight_leptonSF_MU_SF_ID_SYST_UP = m_input.weight_leptonSF_MU_SF_ID_SYST_UP;
  pjt->ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP = m_input.weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
  pjt->ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP = m_input.weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
  pjt->ev_weight_leptonSF_MU_SF_Isol_STAT_UP = m_input.weight_leptonSF_MU_SF_Isol_STAT_UP;
  pjt->ev_weight_leptonSF_MU_SF_Isol_SYST_UP = m_input.weight_leptonSF_MU_SF_Isol_SYST_UP;
  pjt->ev_weight_leptonSF_MU_SF_TTVA_STAT_UP = m_input.weight_leptonSF_MU_SF_TTVA_STAT_UP;
  pjt->ev_weight_leptonSF_MU_SF_TTVA_SYST_UP = m_input.weight_leptonSF_MU_SF_TTVA_SYST_UP;
  if(!isData) {
    pjt->ev_weight_trackjet_bTagSF_DL1r_85 = m_input.weight_trackjet_bTagSF_DL1r_85;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up = m_input.weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_up = m_input.weight_trackjet_bTagSF_DL1r_85_extrapolation_up;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down = m_input.weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_down = m_input.weight_trackjet_bTagSF_DL1r_85_extrapolation_down;
  }
  else {
    pjt->ev_weight_trackjet_bTagSF_DL1r_85 = 1.0;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_up = 1.0;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;
    pjt->ev_weight_trackjet_bTagSF_DL1r_85_extrapolation_down = 1.0;
  }

    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>0.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_1_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(0);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_1_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>1.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_2_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(1);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_2_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>2.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_3_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(2);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_3_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>3.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_4_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(3);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_4_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>4.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_5_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(4);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_5_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>5.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_6_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(5);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_6_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>6.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_7_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(6);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_7_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->size()>7.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_8_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(7);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_8_up = 1.0;

    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->size()>0.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_1_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(0);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_1_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->size()>1.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_2_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(1);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_2_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->size()>2.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_3_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(2);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_3_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->size()>3.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_4_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(3);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_4_up = 1.0;

    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->size()>0.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_1_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(0);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_1_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->size()>1.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_2_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(1);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_2_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->size()>2.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_3_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(2);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_3_up = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->size()>3.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_4_up = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(3);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_4_up = 1.0;


    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>0.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_1_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(0);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_1_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>1.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_2_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(1);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_2_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>2.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_3_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(2);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_3_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>3.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_4_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(3);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_4_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>4.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_5_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(4);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_5_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>5.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_6_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(5);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_6_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>6.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_7_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(6);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_7_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->size()>7.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_8_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(7);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_8_down = 1.0;

    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->size()>0.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_1_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(0);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_1_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->size()>1.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_2_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(1);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_2_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->size()>2.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_3_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(2);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_3_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->size()>3.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_4_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(3);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_4_down = 1.0;

    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->size()>0.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_1_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(0);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_1_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->size()>1.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_2_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(1);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_2_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->size()>2.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_3_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(2);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_3_down = 1.0;
    if(!isData && m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->size()>3.5)
      pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_4_down = m_input.weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(3);
    else pjt->ev_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_4_down = 1.0;

  //weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up
//NANANANA
  unsigned long index1 = m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).at(0);
  unsigned long index2 = m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).at(1);
  unsigned long index3;
  if (m_AssociatedVR.size() > 2.5) {
     index3 = m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).at(2);
  }

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>0.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(0);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_1_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>1.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(1);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_2_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>2.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(2);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_3_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>3.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(3);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_4_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>4.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_5_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(4);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_5_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>5.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_6_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(5);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_6_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>6.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_7_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(6);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_7_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>7.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_8_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(7);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_8_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).size()>8.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_9_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index1).at(8);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_9_up = 1.0;


if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).size()>0.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).at(0);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_1_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).size()>1.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).at(1);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_2_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).size()>2.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).at(2);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_3_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).size()>3.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index1).at(3);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_4_up = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).size()>0.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).at(0);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_1_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).size()>1.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).at(1);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_2_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).size()>2.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).at(2);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_3_up = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).size()>3.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index1).at(3);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_4_up = 1.0;

//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up->at(index1).size()>0.5)
  if (m_input.mcChannelNumber != 0) pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up->at(index1);
  else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_up = 1.0;
//else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_up = 1.0;


//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up->at(index1).size()>0.5)
  if (m_input.mcChannelNumber != 0) pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_from_charm_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up->at(index1);
  else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;
//else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;



if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>0.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(0);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_1_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>1.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(1);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_2_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>2.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(2);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_3_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>3.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(3);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_4_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>4.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_5_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(4);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_5_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>5.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_6_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(5);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_6_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>6.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_7_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(6);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_7_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>7.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_8_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(7);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_8_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).size()>8.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_9_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index2).at(8);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_9_up = 1.0;


if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).size()>0.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).at(0);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_1_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).size()>1.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).at(1);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_2_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).size()>2.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).at(2);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_3_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).size()>3.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index2).at(3);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_4_up = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).size()>0.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).at(0);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_1_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).size()>1.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).at(1);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_2_up = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).size()>2.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).at(2);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_3_up = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).size()>3.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index2).at(3);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_4_up = 1.0;

//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up->at(index2).size()>0.5)
  if (m_input.mcChannelNumber != 0) pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up->at(index2);
  else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_up = 1.0;
//else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_up = 1.0;


//if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up->at(index2).size()>0.5)
  if (m_input.mcChannelNumber != 0) pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_from_charm_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up->at(index2);
  else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;
//else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;


if ( (m_input.mcChannelNumber != 0) && m_AssociatedVR.size() > 2.5){

if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(0);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_1_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>1.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(1);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_2_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>2.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(2);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_3_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>3.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(3);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_4_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>4.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_5_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(4);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_5_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>5.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_6_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(5);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_6_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>6.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_7_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(6);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_7_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>7.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_8_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(7);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_8_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).size()>8.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_9_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up->at(index3).at(8);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_9_up = 1.0;


if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).at(0);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_1_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).size()>1.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).at(1);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_2_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).size()>2.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).at(2);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_3_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).size()>3.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up->at(index3).at(3);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_4_up = 1.0;

if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_1_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).at(0);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_1_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).size()>1.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_2_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).at(1);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_2_up = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).size()>2.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_3_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).at(2);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_3_up = 1.0;

if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).size()>3.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_4_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up->at(index3).at(3);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_4_up = 1.0;

//if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up->at(index3);
//else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_up = 1.0;


//if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_from_charm_up = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up->at(index3);
//else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;
}
else{
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_1_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_2_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_3_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_4_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_5_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_6_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_7_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_8_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_9_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_1_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_2_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_3_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_4_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_1_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_2_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_3_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_4_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_up = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_from_charm_up = 1.0;
}


if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>0.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(0);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_1_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>1.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(1);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_2_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>2.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(2);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_3_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>3.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(3);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_4_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>4.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_5_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(4);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_5_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>5.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_6_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(5);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_6_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>6.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_7_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(6);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_7_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>7.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_8_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(7);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_8_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).size()>8.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_9_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index1).at(8);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_B_9_down = 1.0;


if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).size()>0.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).at(0);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_1_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).size()>1.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).at(1);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_2_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).size()>2.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).at(2);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_3_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).size()>3.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index1).at(3);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_C_4_down = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).size()>0.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).at(0);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_1_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).size()>1.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).at(1);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_2_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).size()>2.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).at(2);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_3_down = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).size()>3.5)
  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index1).at(3);
else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_eigenvars_Light_4_down = 1.0;

//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down->at(index1).size()>0.5)
 if (m_input.mcChannelNumber != 0 )  pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down->at(index1);
 else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_down = 1.0;
//else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_down = 1.0;


//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down->at(index1).size()>0.5)
  if (m_input.mcChannelNumber != 0 )    pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_from_charm_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down->at(index1);
  else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;
//else pjt->ev_weight_perjet_trackjet1_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;



if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>0.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(0);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_1_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>1.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(1);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_2_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>2.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(2);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_3_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>3.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(3);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_4_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>4.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_5_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(4);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_5_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>5.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_6_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(5);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_6_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>6.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_7_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(6);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_7_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>7.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_8_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(7);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_8_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).size()>8.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_9_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index2).at(8);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_B_9_down = 1.0;


if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).size()>0.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).at(0);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_1_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).size()>1.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).at(1);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_2_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).size()>2.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).at(2);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_3_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).size()>3.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index2).at(3);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_C_4_down = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).size()>0.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).at(0);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_1_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).size()>1.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).at(1);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_2_down = 1.0;
if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).size()>2.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).at(2);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_3_down = 1.0;

if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).size()>3.5)
  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index2).at(3);
else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_eigenvars_Light_4_down = 1.0;

//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down->at(index2).size()>0.5)
  if (m_input.mcChannelNumber != 0 )  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down->at(index2);
  else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_down = 1.0;
//else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_down = 1.0;


//if (m_input.mcChannelNumber != 0 && m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down->at(index2).size()>0.5)
  if (m_input.mcChannelNumber != 0 )  pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_from_charm_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down->at(index2);
  else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;
//else pjt->ev_weight_perjet_trackjet2_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;


if (m_input.mcChannelNumber != 0 && m_AssociatedVR.size() > 2.5){

if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(0);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_1_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>1.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(1);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_2_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>2.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(2);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_3_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>3.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(3);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_4_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>4.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_5_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(4);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_5_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>5.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_6_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(5);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_6_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>6.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_7_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(6);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_7_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>7.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_8_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(7);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_8_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).size()>8.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_9_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down->at(index3).at(8);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_9_down = 1.0;


if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).at(0);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_1_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).size()>1.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).at(1);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_2_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).size()>2.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).at(2);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_3_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).size()>3.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down->at(index3).at(3);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_4_down = 1.0;

if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_1_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).at(0);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_1_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).size()>1.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_2_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).at(1);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_2_down = 1.0;
if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).size()>2.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_3_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).at(2);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_3_down = 1.0;

if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).size()>3.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_4_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down->at(index3).at(3);
else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_4_down = 1.0;

//if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down->at(index3);
//else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_down = 1.0;


//if (m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down->at(index3).size()>0.5)
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_from_charm_down = m_input.weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down->at(index3);
//else pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;
}
else{
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_1_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_2_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_3_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_4_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_5_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_6_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_7_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_8_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_B_9_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_1_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_2_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_3_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_C_4_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_1_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_2_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_3_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_eigenvars_Light_4_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_down = 1.0;
  pjt->ev_weight_perjet_trackjet3_bTagSF_DL1r_85_extrapolation_from_charm_down = 1.0;
}


  pjt->ev_weight_pileup_DOWN = m_input.weight_pileup_DOWN;
  pjt->ev_weight_jvt_DOWN = m_input.weight_jvt_DOWN;
  pjt->ev_weight_leptonSF_EL_SF_Trigger_DOWN = m_input.weight_leptonSF_EL_SF_Trigger_DOWN;
  pjt->ev_weight_leptonSF_EL_SF_Reco_DOWN = m_input.weight_leptonSF_EL_SF_Reco_DOWN;
  pjt->ev_weight_leptonSF_EL_SF_ID_DOWN = m_input.weight_leptonSF_EL_SF_ID_DOWN;
  pjt->ev_weight_leptonSF_EL_SF_Isol_DOWN = m_input.weight_leptonSF_EL_SF_Isol_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_Trigger_STAT_DOWN = m_input.weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_Trigger_SYST_DOWN = m_input.weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_ID_STAT_DOWN = m_input.weight_leptonSF_MU_SF_ID_STAT_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_ID_SYST_DOWN = m_input.weight_leptonSF_MU_SF_ID_SYST_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN = m_input.weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN = m_input.weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_Isol_STAT_DOWN = m_input.weight_leptonSF_MU_SF_Isol_STAT_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_Isol_SYST_DOWN = m_input.weight_leptonSF_MU_SF_Isol_SYST_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_TTVA_STAT_DOWN = m_input.weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
  pjt->ev_weight_leptonSF_MU_SF_TTVA_SYST_DOWN = m_input.weight_leptonSF_MU_SF_TTVA_SYST_DOWN;


  //save hadronization information //disable currently, L.
  // if (!isData)
  // {
  //   pjt->hadName = m_hadronizationProvider->gethadName(m_input.mcChannelNumber);
  // }
  // else
  // {
  //   pjt->hadName = "data";
  // }

  //in order to do a proper background subtraction we need to flag the events as coming from ttbar (signal) or non-ttbar ??
  pjt->isSignal = m_isSignal;
  pjt->sample = m_sample;

  //fill the MVA score
  // pjt->mva_score = m_MVATree->mva_score; //L: no MVA now
  // pjt->mva_score = -1;

  //L: add DXbb and others for testing
  //L: remeber modify the "ProbeJet" defination in another package!
if (m_input.mcChannelNumber != 0){
  pjt->DXbb_Top = weight_perAssociatedVR_DL1r85->at(0); 
  pjt->DXbb_QCD = weight_perAssociatedVR_DL1r85->at(1);
  if (m_AssociatedVR.size() > 2.5) pjt->DXbb_Higgs = weight_perAssociatedVR_DL1r85->at(2);
  else pjt->DXbb_Higgs = 1.0;
}
else{
pjt->DXbb_Top = 1.0;
pjt->DXbb_QCD = 1.0;
pjt->DXbb_Higgs = 1.0;

}
  pjt->DXbb_Top_v3 = m_input.ljet_m_Xbb2020v3_Top->at(m_jetIndex_boosted_top);
  pjt->DXbb_QCD_v3 = m_input.ljet_m_Xbb2020v3_QCD->at(m_jetIndex_boosted_top);
  pjt->DXbb_Higgs_v3 = m_input.ljet_m_Xbb2020v3_Higgs->at(m_jetIndex_boosted_top);

  int probe_jet_index2 = -1;
  int tag_jet_index2 = -1;

    tag_jet_index2 = m_jetIndex_boosted_b_lep;
    probe_jet_index2 = m_jetIndex_boosted_top;

  const auto &pjT = m_ljets.at(probe_jet_index2);
  // store probe jet histograms
    pjt->probe_pt=pjT.Pt() / 1.0e3; //matched large-R jet
    pjt->probe_eta=pjT.Eta(); //matched large-R jet
    pjt->probe_phi=pjT.Phi(); //matched large-R jet
    pjt->probe_m=pjT.M() / 1.0e3; //matched large-R jet


  if (tag_jet_index2 > -1)
  { 
    const auto &tjT = m_jets.at(tag_jet_index2); 
    pjt->tag_pt=tjT.Pt() / 1.0e3;
    pjt->tag_eta=tjT.Eta();
    pjt->tag_phi=tjT.Phi();
    pjt->tag_m=tjT.M() / 1.0e3;
    pjt->tag_mlj=get_mlj() / 1.0e3;
  }


   if (isData)
   {
     pjt->ljet_label = 0; //L: ???
   }
   else
   {
     pjt->ljet_label = m_input.ljet_truthLabel->at(m_jetIndex_boosted_top);
   }
  //if (ljet_truthLabel->size()>0) {
  //  pjt->ljet1_label = m_input.ljet_truthLabel->at(0);
  //  if (ljet_truthLabel->size()>1) {
  //    pjt->ljet2_label = m_input.ljet_truthLabel->at(1);
  //    if (ljet_truthLabel->size()>2) {
  //      pjt->ljet3_label = m_input.ljet_truthLabel->at(2);
  //    }
  //  }
  //}

  pjt->mcChannelNumber = m_input.mcChannelNumber;

  //leading large-R truth (not always matched)
  //int nVR=m_input.ljet1_ghostVR_trkjet_raw_index->size();
  //int nVR=m_input.ljet_ghostVR_indices[m_jetIndex_boosted_top].size();
  int nVR=m_input.ljet_ghostVR_indices->at(m_jetIndex_boosted_top).size();
  pjt->n_ljet1_VRjet=nVR;

if(m_AssociatedVR.size()>0){
  pjt->ljet1_VR1_Pt = m_AssociatedVR.at(0).Pt() / 1.0e3;
  pjt->ljet1_VR1_Eta = m_AssociatedVR.at(0).Eta();
  pjt->ljet1_VR1_Phi = m_AssociatedVR.at(0).Phi();
  if(m_AssociatedVR.size()>1){
    pjt->ljet1_VR2_Pt = m_AssociatedVR.at(1).Pt() / 1.0e3;
    pjt->ljet1_VR2_Eta = m_AssociatedVR.at(1).Eta();
    pjt->ljet1_VR2_Phi = m_AssociatedVR.at(1).Phi();
    if(m_AssociatedVR.size()>2){
      pjt->ljet1_VR3_Pt = m_AssociatedVR.at(2).Pt() / 1.0e3;
      pjt->ljet1_VR3_Eta = m_AssociatedVR.at(2).Eta();
      pjt->ljet1_VR3_Phi = m_AssociatedVR.at(2).Phi();
    }
  }
}


//  pjt->ljet1_VR1_label = -9999;
//  pjt->ljet1_VR2_label = -9999;
//  pjt->ljet1_VR3_label = -9999;
//  pjt->ljet1_VR4_label = -9999;
//  pjt->ljet1_VR1_labelExt = -9999;
//  pjt->ljet1_VR2_labelExt = -9999;
//  pjt->ljet1_VR3_labelExt = -9999;
//  pjt->ljet1_VR4_labelExt = -9999;

//  if (nVR > 0)
//  {
//    //pjt->ljet1_VR1_label = m_input.ljet1_ghostVR_trkjet_truthflav->at(0);
//    //pjt->ljet1_VR1_labelExt = m_input.ljet1_ghostVR_trkjet_truthflavExtended->at(0);
//    pjt->ljet1_VR1_label = m_input.tjet_truthflav->at(ljet1_ghostVR_trkjet_index->at(0));
//    pjt->ljet1_VR1_labelExt = m_input.tjet_truthflavExtended->at(ljet1_ghostVR_trkjet_index->at(0));
//  }
//  if (nVR > 1)
//  {
//    pjt->ljet1_VR1_label = m_input.tjet_truthflav->at(ljet1_ghostVR_trkjet_index->at(1));
//    pjt->ljet1_VR1_labelExt = m_input.tjet_truthflavExtended->at(ljet1_ghostVR_trkjet_index->at(1));
//  }
//  if (nVR > 2)
//  {
//    pjt->ljet1_VR1_label = m_input.tjet_truthflav->at(ljet1_ghostVR_trkjet_index->at(2));
//    pjt->ljet1_VR1_labelExt = m_input.tjet_truthflavExtended->at(ljet1_ghostVR_trkjet_index->at(2));
//  }
//    if (nVR > 3)
//  {
//    pjt->ljet1_VR1_label = m_input.tjet_truthflav->at(ljet1_ghostVR_trkjet_index->at(3));
//    pjt->ljet1_VR1_labelExt = m_input.tjet_truthflavExtended->at(ljet1_ghostVR_trkjet_index->at(3));
//  }

  int isVHbbTaggedTree=0;
  if(m_AssociatedVR.size()==2) isVHbbTaggedTree=isVHbbTagged_perAssociatedVR->at(0)+isVHbbTagged_perAssociatedVR->at(1);
  else if(m_AssociatedVR.size()>2) isVHbbTaggedTree=isVHbbTagged_perAssociatedVR->at(0)+isVHbbTagged_perAssociatedVR->at(1)+isVHbbTagged_perAssociatedVR->at(2);
  pjt->nVHbbTagged=isVHbbTaggedTree; //NINIHERE
  

  //fill the tree
  pjt->tree->Fill();
  return EL::StatusCode::SUCCESS;
}

