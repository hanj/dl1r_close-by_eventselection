#ifndef INPUT_TREE_H
#define INPUT_TREE_H

//cpp includes
#include <vector>
#include <iostream>

//ROOT includes
#include <TTree.h>
#include <TBranch.h>

class InputTree
{

public:
  ///////////////////////////////////////////////////////////////////////////
  // Structure of the input tree (basically copied from tree->MakeClass()
  ///////////////////////////////////////////////////////////////////////////
  // Declaration of leaf types

  // Declaration of leaf types
  //NTUPLESv1
  //mc type
  std::vector<float> *mc_generator_weights;
  Float_t weight_mc;
  Float_t weight_pileup;
  Float_t weight_leptonSF;
  Float_t weight_globalLeptonTriggerSF;
  Float_t weight_oldTriggerSF;
  Float_t weight_bTagSF_DL1_77;
  Float_t weight_bTagSF_DL1r_77;
  Float_t weight_trackjet_bTagSF_DL1_77;
  Float_t weight_trackjet_bTagSF_DL1r_85;
  std::vector<float>   *weight_perjet_trackjet_bTagSF_DL1r_85;

  Float_t weight_jvt;
  Float_t weight_pileup_UP;
  Float_t weight_pileup_DOWN;
  Float_t weight_leptonSF_EL_SF_Trigger_UP;
  Float_t weight_leptonSF_EL_SF_Trigger_DOWN;
  Float_t weight_leptonSF_EL_SF_Reco_UP;
  Float_t weight_leptonSF_EL_SF_Reco_DOWN;
  Float_t weight_leptonSF_EL_SF_ID_UP;
  Float_t weight_leptonSF_EL_SF_ID_DOWN;
  Float_t weight_leptonSF_EL_SF_Isol_UP;
  Float_t weight_leptonSF_EL_SF_Isol_DOWN;
  Float_t weight_leptonSF_MU_SF_Trigger_STAT_UP;
  Float_t weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
  Float_t weight_leptonSF_MU_SF_Trigger_SYST_UP;
  Float_t weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
  Float_t weight_leptonSF_MU_SF_ID_STAT_UP;
  Float_t weight_leptonSF_MU_SF_ID_STAT_DOWN;
  Float_t weight_leptonSF_MU_SF_ID_SYST_UP;
  Float_t weight_leptonSF_MU_SF_ID_SYST_DOWN;
  Float_t weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
  Float_t weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
  Float_t weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
  Float_t weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
  Float_t weight_leptonSF_MU_SF_Isol_STAT_UP;
  Float_t weight_leptonSF_MU_SF_Isol_STAT_DOWN;
  Float_t weight_leptonSF_MU_SF_Isol_SYST_UP;
  Float_t weight_leptonSF_MU_SF_Isol_SYST_DOWN;
  Float_t weight_leptonSF_MU_SF_TTVA_STAT_UP;
  Float_t weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
  Float_t weight_leptonSF_MU_SF_TTVA_SYST_UP;
  Float_t weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
  Float_t weight_globalLeptonTriggerSF_EL_Trigger_UP;
  Float_t weight_globalLeptonTriggerSF_EL_Trigger_DOWN;
  Float_t weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;
  Float_t weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;
  Float_t weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;
  Float_t weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;
  Float_t weight_oldTriggerSF_EL_Trigger_UP;
  Float_t weight_oldTriggerSF_EL_Trigger_DOWN;
  Float_t weight_oldTriggerSF_MU_Trigger_STAT_UP;
  Float_t weight_oldTriggerSF_MU_Trigger_STAT_DOWN;
  Float_t weight_oldTriggerSF_MU_Trigger_SYST_UP;
  Float_t weight_oldTriggerSF_MU_Trigger_SYST_DOWN;
  Float_t weight_indiv_SF_EL_Reco;
  Float_t weight_indiv_SF_EL_Reco_UP;
  Float_t weight_indiv_SF_EL_Reco_DOWN;
  Float_t weight_indiv_SF_EL_ID;
  Float_t weight_indiv_SF_EL_ID_UP;
  Float_t weight_indiv_SF_EL_ID_DOWN;
  Float_t weight_indiv_SF_EL_Isol;
  Float_t weight_indiv_SF_EL_Isol_UP;
  Float_t weight_indiv_SF_EL_Isol_DOWN;
  Float_t weight_indiv_SF_EL_ChargeID;
  Float_t weight_indiv_SF_EL_ChargeID_UP;
  Float_t weight_indiv_SF_EL_ChargeID_DOWN;
  Float_t weight_indiv_SF_EL_ChargeMisID;
  Float_t weight_indiv_SF_EL_ChargeMisID_STAT_UP;
  Float_t weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
  Float_t weight_indiv_SF_EL_ChargeMisID_SYST_UP;
  Float_t weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
  Float_t weight_indiv_SF_MU_ID;
  Float_t weight_indiv_SF_MU_ID_STAT_UP;
  Float_t weight_indiv_SF_MU_ID_STAT_DOWN;
  Float_t weight_indiv_SF_MU_ID_SYST_UP;
  Float_t weight_indiv_SF_MU_ID_SYST_DOWN;
  Float_t weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
  Float_t weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
  Float_t weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
  Float_t weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
  Float_t weight_indiv_SF_MU_Isol;
  Float_t weight_indiv_SF_MU_Isol_STAT_UP;
  Float_t weight_indiv_SF_MU_Isol_STAT_DOWN;
  Float_t weight_indiv_SF_MU_Isol_SYST_UP;
  Float_t weight_indiv_SF_MU_Isol_SYST_DOWN;
  Float_t weight_indiv_SF_MU_TTVA;
  Float_t weight_indiv_SF_MU_TTVA_STAT_UP;
  Float_t weight_indiv_SF_MU_TTVA_STAT_DOWN;
  Float_t weight_indiv_SF_MU_TTVA_SYST_UP;
  Float_t weight_indiv_SF_MU_TTVA_SYST_DOWN;
  Float_t weight_jvt_UP;
  Float_t weight_jvt_DOWN;
  std::vector<float> *weight_bTagSF_DL1_77_eigenvars_B_up;
  std::vector<float> *weight_bTagSF_DL1_77_eigenvars_C_up;
  std::vector<float> *weight_bTagSF_DL1_77_eigenvars_Light_up;
  std::vector<float> *weight_bTagSF_DL1_77_eigenvars_B_down;
  std::vector<float> *weight_bTagSF_DL1_77_eigenvars_C_down;
  std::vector<float> *weight_bTagSF_DL1_77_eigenvars_Light_down;
  Float_t weight_bTagSF_DL1_77_extrapolation_up;
  Float_t weight_bTagSF_DL1_77_extrapolation_down;
  Float_t weight_bTagSF_DL1_77_extrapolation_from_charm_up;
  Float_t weight_bTagSF_DL1_77_extrapolation_from_charm_down;
  std::vector<float> *weight_bTagSF_DL1r_77_eigenvars_B_up;
  std::vector<float> *weight_bTagSF_DL1r_77_eigenvars_C_up;
  std::vector<float> *weight_bTagSF_DL1r_77_eigenvars_Light_up;
  std::vector<float> *weight_bTagSF_DL1r_77_eigenvars_B_down;
  std::vector<float> *weight_bTagSF_DL1r_77_eigenvars_C_down;
  std::vector<float> *weight_bTagSF_DL1r_77_eigenvars_Light_down;
  Float_t weight_bTagSF_DL1r_77_extrapolation_up;
  Float_t weight_bTagSF_DL1r_77_extrapolation_down;
  Float_t weight_bTagSF_DL1r_77_extrapolation_from_charm_up;
  Float_t weight_bTagSF_DL1r_77_extrapolation_from_charm_down;
  std::vector<float> *weight_trackjet_bTagSF_DL1_77_eigenvars_B_up;
  std::vector<float> *weight_trackjet_bTagSF_DL1_77_eigenvars_C_up;
  std::vector<float> *weight_trackjet_bTagSF_DL1_77_eigenvars_Light_up;
  std::vector<float> *weight_trackjet_bTagSF_DL1_77_eigenvars_B_down;
  std::vector<float> *weight_trackjet_bTagSF_DL1_77_eigenvars_C_down;
  std::vector<float> *weight_trackjet_bTagSF_DL1_77_eigenvars_Light_down;

  std::vector<float> *weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up;
  std::vector<float> *weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up;
  std::vector<float> *weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up;
  std::vector<float> *weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down;
  std::vector<float> *weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down;
  std::vector<float> *weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down;
  Float_t weight_trackjet_bTagSF_DL1r_85_extrapolation_up;
  Float_t weight_trackjet_bTagSF_DL1r_85_extrapolation_down;
  Float_t weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up;
  Float_t weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down;

  std::vector<std::vector<float>> *weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up;
  std::vector<std::vector<float>> *weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down;
  std::vector<std::vector<float>> *weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up;
  std::vector<std::vector<float>> *weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down;
  std::vector<std::vector<float>> *weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up;
  std::vector<std::vector<float>> *weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down;
  std::vector<float>   *weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up;
  std::vector<float>   *weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down;
  std::vector<float>   *weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up;
  std::vector<float>   *weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down;



  Float_t weight_trackjet_bTagSF_DL1_77_extrapolation_up;
  Float_t weight_trackjet_bTagSF_DL1_77_extrapolation_down;
  Float_t weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_up;
  Float_t weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_down;


  UInt_t randomRunNumber;

  std::vector<int> *el_true_type;
  std::vector<int> *el_true_origin;
  std::vector<int> *el_true_firstEgMotherTruthType;
  std::vector<int> *el_true_firstEgMotherTruthOrigin;
  std::vector<int> *el_true_firstEgMotherPdgId;
  std::vector<char> *el_true_isPrompt;
  std::vector<char> *el_true_isChargeFl;

  std::vector<int> *mu_true_type;
  std::vector<int> *mu_true_origin;
  std::vector<char> *mu_true_isPrompt;

  std::vector<int> *jet_truthflav;
  std::vector<int> *jet_truthPartonLabel;
  std::vector<char> *jet_isTrueHS;
  std::vector<int> *jet_truthflavExtended;
  std::vector<int> *tjet_truthflav;
  std::vector<int> *tjet_truthflavExtended;

  std::vector<int> *ljet_truthLabel;

  //common type
  ULong64_t eventNumber;
  UInt_t runNumber;
  UInt_t mcChannelNumber;
  Float_t mu;
  Float_t mu_actual;
  UInt_t backgroundFlags;
  UInt_t hasBadMuon;
  std::vector<float> *el_pt;
  std::vector<float> *el_eta;
  std::vector<float> *el_cl_eta;
  std::vector<float> *el_phi;
  std::vector<float> *el_e;
  std::vector<float> *el_charge;
  std::vector<float> *el_topoetcone20;
  std::vector<float> *el_ptvarcone20;
  std::vector<char> *el_isTight;
  std::vector<char> *el_CF;
  std::vector<float> *el_d0sig;
  std::vector<float> *el_delta_z0_sintheta;

  std::vector<float> *mu_pt;
  std::vector<float> *mu_eta;
  std::vector<float> *mu_phi;
  std::vector<float> *mu_e;
  std::vector<float> *mu_charge;
  std::vector<float> *mu_topoetcone20;
  std::vector<float> *mu_ptvarcone30;
  std::vector<char> *mu_isTight;
  std::vector<float> *mu_d0sig;
  std::vector<float> *mu_delta_z0_sintheta;

  std::vector<float> *jet_pt;
  std::vector<float> *jet_eta;
  std::vector<float> *jet_phi;
  std::vector<float> *jet_e;
  std::vector<float> *jet_mv2c10;
  std::vector<float> *jet_jvt;
  std::vector<char> *jet_passfjvt;

  std::vector<char> *jet_isbtagged_DL1_77;
  std::vector<char> *jet_isbtagged_DL1r_60;
  std::vector<char> *jet_isbtagged_DL1r_70;
  std::vector<char> *jet_isbtagged_DL1r_77;
  std::vector<char> *jet_isbtagged_DL1r_85;
  std::vector<float> *jet_DL1;
  std::vector<float> *jet_DL1r;
  std::vector<float> *jet_DL1rmu;
  std::vector<float> *jet_DL1_pu;
  std::vector<float> *jet_DL1_pc;
  std::vector<float> *jet_DL1_pb;
  std::vector<float> *jet_DL1r_pu;
  std::vector<float> *jet_DL1r_pc;
  std::vector<float> *jet_DL1r_pb;
  std::vector<float> *jet_DL1rmu_pu;
  std::vector<float> *jet_DL1rmu_pc;
  std::vector<float> *jet_DL1rmu_pb;
  std::vector<float> *ljet_pt;
  std::vector<float> *ljet_eta;
  std::vector<float> *ljet_phi;
  std::vector<float> *ljet_e;
  std::vector<float> *ljet_m;
  std::vector<float> *ljet_sd12;

  std::vector<float> *tjet_pt;
  std::vector<float> *tjet_eta;
  std::vector<float> *tjet_phi;
  std::vector<float> *tjet_e;
  std::vector<float> *tjet_mv2c00;
  std::vector<float> *tjet_mv2c10;
  std::vector<float> *tjet_mv2c20;
  std::vector<float> *tjet_DL1;
  std::vector<float> *tjet_DL1r;
  std::vector<float> *tjet_DL1rmu;
  std::vector<char> *tjet_isbtagged_DL1_77;
  std::vector<char> *tjet_isbtagged_DL1r_60;
  std::vector<char> *tjet_isbtagged_DL1r_70;
  std::vector<char> *tjet_isbtagged_DL1r_77;
  std::vector<char> *tjet_isbtagged_DL1r_85;
  Float_t met_met;
  Float_t met_phi;
  Int_t efatjets;
  Int_t mufatjets;
  Char_t HLT_mu50;
  Char_t HLT_mu26_ivarmedium;
  Char_t HLT_e140_lhloose_nod0;
  Char_t HLT_e26_lhtight_nod0_ivarloose;
  Char_t HLT_e120_lhloose;
  Char_t HLT_mu20_iloose_L1MU15;
  Char_t HLT_e24_lhmedium_L1EM20VH;
  Char_t HLT_e60_lhmedium;
  Char_t HLT_e60_lhmedium_nod0;
  std::vector<char> *el_trigMatch_HLT_e60_lhmedium_nod0;
  std::vector<char> *el_trigMatch_HLT_e60_lhmedium;
  std::vector<char> *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
  std::vector<char> *el_trigMatch_HLT_e120_lhloose;
  std::vector<char> *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
  std::vector<char> *el_trigMatch_HLT_e300_etcut;
  std::vector<char> *el_trigMatch_HLT_e140_lhloose_nod0;
  std::vector<char> *mu_trigMatch_HLT_mu50;
  std::vector<char> *mu_trigMatch_HLT_mu20_iloose_L1MU15;
  std::vector<char> *mu_trigMatch_HLT_mu26_ivarmedium;
  std::vector<float> *ljet_SubjetBScore_Top;
  std::vector<float> *ljet_SubjetBScore_QCD;
  std::vector<float> *ljet_SubjetBScore_Higgs;
  std::vector<float> *ljet_Xbb202006_Top;
  std::vector<float> *ljet_Xbb202006_QCD;
  std::vector<float> *ljet_Xbb202006_Higgs;
  std::vector<float> *ljet_m_Xbb2020v3_Top;
  std::vector<float> *ljet_m_Xbb2020v3_QCD;
  std::vector<float> *ljet_m_Xbb2020v3_Higgs;
  std::vector<float> *jet_mass;
  std::vector<unsigned long> *tjet_raw_index;
  std::vector<float> *ljet1_ghostVR_trkjet_m;
  std::vector<float> *ljet1_ghostVR_trkjet_pt;
  std::vector<float> *ljet1_ghostVR_trkjet_eta;
  std::vector<float> *ljet1_ghostVR_trkjet_phi;
  std::vector<float> *ljet1_ghostVR_trkjet_e;
  std::vector<float> *ljet1_ghostVR_trkjet_DL1r;
  std::vector<float> *ljet1_ghostVR_trkjet_DL1r_pu;
  std::vector<float> *ljet1_ghostVR_trkjet_DL1r_pc;
  std::vector<float> *ljet1_ghostVR_trkjet_DL1r_pb;
  std::vector<int> *ljet1_ghostVR_trkjet_truthflav;
  std::vector<int> *ljet1_ghostVR_trkjet_truthflavExtended;
  std::vector<unsigned long> *ljet1_ghostVR_trkjet_raw_index;
  std::vector<unsigned long> *ljet1_ghostVR_trkjet_index;
  std::vector<std::vector<unsigned long>> *ljet_ghostVR_indices;
  std::vector<std::vector<unsigned long>> *ljet_ghostVR_indices_raw;

  //data type
  Float_t mu_original_xAOD;
  Float_t mu_actual_original_xAOD;

private:
  // List of branches
  //mc
  TBranch *b_mc_generator_weights;                                        //!
  TBranch *b_weight_mc;                                                   //!
  TBranch *b_weight_pileup;                                               //!
  TBranch *b_weight_leptonSF;                                             //!
  TBranch *b_weight_globalLeptonTriggerSF;                                //!
  TBranch *b_weight_oldTriggerSF;                                         //!
  TBranch *b_weight_bTagSF_DL1_77;                                        //!
  TBranch *b_weight_bTagSF_DL1r_77;                                       //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77;                               //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85;                               //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85;                               //!
  TBranch *b_weight_jvt;                                                  //!
  TBranch *b_weight_pileup_UP;                                            //!
  TBranch *b_weight_pileup_DOWN;                                          //!
  TBranch *b_weight_leptonSF_EL_SF_Trigger_UP;                            //!
  TBranch *b_weight_leptonSF_EL_SF_Trigger_DOWN;                          //!
  TBranch *b_weight_leptonSF_EL_SF_Reco_UP;                               //!
  TBranch *b_weight_leptonSF_EL_SF_Reco_DOWN;                             //!
  TBranch *b_weight_leptonSF_EL_SF_ID_UP;                                 //!
  TBranch *b_weight_leptonSF_EL_SF_ID_DOWN;                               //!
  TBranch *b_weight_leptonSF_EL_SF_Isol_UP;                               //!
  TBranch *b_weight_leptonSF_EL_SF_Isol_DOWN;                             //!
  TBranch *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;                       //!
  TBranch *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;                     //!
  TBranch *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;                       //!
  TBranch *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;                     //!
  TBranch *b_weight_leptonSF_MU_SF_ID_STAT_UP;                            //!
  TBranch *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;                          //!
  TBranch *b_weight_leptonSF_MU_SF_ID_SYST_UP;                            //!
  TBranch *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;                          //!
  TBranch *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;                      //!
  TBranch *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;                    //!
  TBranch *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;                      //!
  TBranch *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;                    //!
  TBranch *b_weight_leptonSF_MU_SF_Isol_STAT_UP;                          //!
  TBranch *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;                        //!
  TBranch *b_weight_leptonSF_MU_SF_Isol_SYST_UP;                          //!
  TBranch *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;                        //!
  TBranch *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;                          //!
  TBranch *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;                        //!
  TBranch *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;                          //!
  TBranch *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;                        //!
  TBranch *b_weight_globalLeptonTriggerSF_EL_Trigger_UP;                  //!
  TBranch *b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;                //!
  TBranch *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;             //!
  TBranch *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;           //!
  TBranch *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;             //!
  TBranch *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;           //!
  TBranch *b_weight_oldTriggerSF_EL_Trigger_UP;                           //!
  TBranch *b_weight_oldTriggerSF_EL_Trigger_DOWN;                         //!
  TBranch *b_weight_oldTriggerSF_MU_Trigger_STAT_UP;                      //!
  TBranch *b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;                    //!
  TBranch *b_weight_oldTriggerSF_MU_Trigger_SYST_UP;                      //!
  TBranch *b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;                    //!
  TBranch *b_weight_indiv_SF_EL_Reco;                                     //!
  TBranch *b_weight_indiv_SF_EL_Reco_UP;                                  //!
  TBranch *b_weight_indiv_SF_EL_Reco_DOWN;                                //!
  TBranch *b_weight_indiv_SF_EL_ID;                                       //!
  TBranch *b_weight_indiv_SF_EL_ID_UP;                                    //!
  TBranch *b_weight_indiv_SF_EL_ID_DOWN;                                  //!
  TBranch *b_weight_indiv_SF_EL_Isol;                                     //!
  TBranch *b_weight_indiv_SF_EL_Isol_UP;                                  //!
  TBranch *b_weight_indiv_SF_EL_Isol_DOWN;                                //!
  TBranch *b_weight_indiv_SF_EL_ChargeID;                                 //!
  TBranch *b_weight_indiv_SF_EL_ChargeID_UP;                              //!
  TBranch *b_weight_indiv_SF_EL_ChargeID_DOWN;                            //!
  TBranch *b_weight_indiv_SF_EL_ChargeMisID;                              //!
  TBranch *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;                      //!
  TBranch *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;                    //!
  TBranch *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;                      //!
  TBranch *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;                    //!
  TBranch *b_weight_indiv_SF_MU_ID;                                       //!
  TBranch *b_weight_indiv_SF_MU_ID_STAT_UP;                               //!
  TBranch *b_weight_indiv_SF_MU_ID_STAT_DOWN;                             //!
  TBranch *b_weight_indiv_SF_MU_ID_SYST_UP;                               //!
  TBranch *b_weight_indiv_SF_MU_ID_SYST_DOWN;                             //!
  TBranch *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;                         //!
  TBranch *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;                       //!
  TBranch *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;                         //!
  TBranch *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;                       //!
  TBranch *b_weight_indiv_SF_MU_Isol;                                     //!
  TBranch *b_weight_indiv_SF_MU_Isol_STAT_UP;                             //!
  TBranch *b_weight_indiv_SF_MU_Isol_STAT_DOWN;                           //!
  TBranch *b_weight_indiv_SF_MU_Isol_SYST_UP;                             //!
  TBranch *b_weight_indiv_SF_MU_Isol_SYST_DOWN;                           //!
  TBranch *b_weight_indiv_SF_MU_TTVA;                                     //!
  TBranch *b_weight_indiv_SF_MU_TTVA_STAT_UP;                             //!
  TBranch *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;                           //!
  TBranch *b_weight_indiv_SF_MU_TTVA_SYST_UP;                             //!
  TBranch *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;                           //!
  TBranch *b_weight_jvt_UP;                                               //!
  TBranch *b_weight_jvt_DOWN;                                             //!
  TBranch *b_weight_bTagSF_DL1_77_eigenvars_B_up;                         //!
  TBranch *b_weight_bTagSF_DL1_77_eigenvars_C_up;                         //!
  TBranch *b_weight_bTagSF_DL1_77_eigenvars_Light_up;                     //!
  TBranch *b_weight_bTagSF_DL1_77_eigenvars_B_down;                       //!
  TBranch *b_weight_bTagSF_DL1_77_eigenvars_C_down;                       //!
  TBranch *b_weight_bTagSF_DL1_77_eigenvars_Light_down;                   //!
  TBranch *b_weight_bTagSF_DL1_77_extrapolation_up;                       //!
  TBranch *b_weight_bTagSF_DL1_77_extrapolation_down;                     //!
  TBranch *b_weight_bTagSF_DL1_77_extrapolation_from_charm_up;            //!
  TBranch *b_weight_bTagSF_DL1_77_extrapolation_from_charm_down;          //!
  TBranch *b_weight_bTagSF_DL1r_77_eigenvars_B_up;                        //!
  TBranch *b_weight_bTagSF_DL1r_77_eigenvars_C_up;                        //!
  TBranch *b_weight_bTagSF_DL1r_77_eigenvars_Light_up;                    //!
  TBranch *b_weight_bTagSF_DL1r_77_eigenvars_B_down;                      //!
  TBranch *b_weight_bTagSF_DL1r_77_eigenvars_C_down;                      //!
  TBranch *b_weight_bTagSF_DL1r_77_eigenvars_Light_down;                  //!
  TBranch *b_weight_bTagSF_DL1r_77_extrapolation_up;                      //!
  TBranch *b_weight_bTagSF_DL1r_77_extrapolation_down;                    //!
  TBranch *b_weight_bTagSF_DL1r_77_extrapolation_from_charm_up;           //!
  TBranch *b_weight_bTagSF_DL1r_77_extrapolation_from_charm_down;         //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_eigenvars_B_up;                //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_eigenvars_C_up;                //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_eigenvars_Light_up;            //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_eigenvars_B_down;              //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_eigenvars_C_down;              //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_eigenvars_Light_down;          //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_extrapolation_up;              //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_extrapolation_down;            //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_up;   //!
  TBranch *b_weight_trackjet_bTagSF_DL1_77_extrapolation_from_charm_down; //!

  TBranch *b_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_up;                //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_up;                //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_up;            //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_eigenvars_B_down;              //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_eigenvars_C_down;              //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_eigenvars_Light_down;          //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_extrapolation_up;              //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_extrapolation_down;            //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up;   //!
  TBranch *b_weight_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down; //!

  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up;                //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up;                //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up;            //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down;              //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down;              //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down;          //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_up;              //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_down;            //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up;   //!
  TBranch *b_weight_perjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down; //!



  TBranch *b_randomRunNumber; //!

  TBranch *b_el_true_type;                     //!
  TBranch *b_el_true_origin;                   //!
  TBranch *b_el_true_firstEgMotherTruthType;   //!
  TBranch *b_el_true_firstEgMotherTruthOrigin; //!
  TBranch *b_el_true_firstEgMotherPdgId;       //!
  TBranch *b_el_true_isPrompt;                 //!
  TBranch *b_el_true_isChargeFl;               //!

  TBranch *b_mu_true_type;     //!
  TBranch *b_mu_true_origin;   //!
  TBranch *b_mu_true_isPrompt; //!

  TBranch *b_jet_truthflav;         //!
  TBranch *b_jet_truthPartonLabel;  //!
  TBranch *b_jet_isTrueHS;          //!
  TBranch *b_jet_truthflavExtended; //!
  TBranch *b_tjet_truthflav; //!
  TBranch *b_tjet_truthflavExtended; //!

  TBranch *b_ljet_truthLabel; //!

  //common
  TBranch *b_eventNumber;          //!
  TBranch *b_runNumber;            //!
  TBranch *b_mcChannelNumber;      //!
  TBranch *b_mu;                   //!
  TBranch *b_mu_actual;            //!
  TBranch *b_backgroundFlags;      //!
  TBranch *b_hasBadMuon;           //!
  TBranch *b_el_pt;                //!
  TBranch *b_el_eta;               //!
  TBranch *b_el_cl_eta;            //!
  TBranch *b_el_phi;               //!
  TBranch *b_el_e;                 //!
  TBranch *b_el_charge;            //!
  TBranch *b_el_topoetcone20;      //!
  TBranch *b_el_ptvarcone20;       //!
  TBranch *b_el_isTight;           //!
  TBranch *b_el_CF;                //!
  TBranch *b_el_d0sig;             //!
  TBranch *b_el_delta_z0_sintheta; //!

  TBranch *b_mu_pt;                //!
  TBranch *b_mu_eta;               //!
  TBranch *b_mu_phi;               //!
  TBranch *b_mu_e;                 //!
  TBranch *b_mu_charge;            //!
  TBranch *b_mu_topoetcone20;      //!
  TBranch *b_mu_ptvarcone30;       //!
  TBranch *b_mu_isTight;           //!
  TBranch *b_mu_d0sig;             //!
  TBranch *b_mu_delta_z0_sintheta; //!

  TBranch *b_jet_pt;       //!
  TBranch *b_jet_eta;      //!
  TBranch *b_jet_phi;      //!
  TBranch *b_jet_e;        //!
  TBranch *b_jet_mv2c10;   //!
  TBranch *b_jet_jvt;      //!
  TBranch *b_jet_passfjvt; //!

  TBranch *b_jet_isbtagged_DL1_77;  //!
  TBranch *b_jet_isbtagged_DL1r_77; //!
  TBranch *b_jet_isbtagged_DL1r_60; //!
  TBranch *b_jet_isbtagged_DL1r_70; //!
  TBranch *b_jet_isbtagged_DL1r_85; //!
  TBranch *b_jet_DL1;               //!
  TBranch *b_jet_DL1r;              //!
  TBranch *b_jet_DL1rmu;            //!
  TBranch *b_jet_DL1_pu;            //!
  TBranch *b_jet_DL1_pc;            //!
  TBranch *b_jet_DL1_pb;            //!
  TBranch *b_jet_DL1r_pu;           //!
  TBranch *b_jet_DL1r_pc;           //!
  TBranch *b_jet_DL1r_pb;           //!
  TBranch *b_jet_DL1rmu_pu;         //!
  TBranch *b_jet_DL1rmu_pc;         //!
  TBranch *b_jet_DL1rmu_pb;         //!
  TBranch *b_ljet_pt;               //!
  TBranch *b_ljet_eta;              //!
  TBranch *b_ljet_phi;              //!
  TBranch *b_ljet_e;                //!
  TBranch *b_ljet_m;                //!
  TBranch *b_ljet_sd12;             //!

  TBranch *b_tjet_pt;                                     //!
  TBranch *b_tjet_eta;                                    //!
  TBranch *b_tjet_phi;                                    //!
  TBranch *b_tjet_e;                                      //!
  TBranch *b_tjet_mv2c00;                                 //!
  TBranch *b_tjet_mv2c10;                                 //!
  TBranch *b_tjet_mv2c20;                                 //!
  TBranch *b_tjet_DL1;                                    //!
  TBranch *b_tjet_DL1r;                                   //!
  TBranch *b_tjet_DL1rmu;                                 //!
  TBranch *b_tjet_isbtagged_DL1_77;                       //!
  TBranch *b_tjet_isbtagged_DL1r_77;                      //!
  TBranch *b_tjet_isbtagged_DL1r_85;                      //!
  TBranch *b_tjet_isbtagged_DL1r_70;                      //!
  TBranch *b_tjet_isbtagged_DL1r_60;                      //!
  TBranch *b_met_met;                                     //!
  TBranch *b_met_phi;                                     //!
  TBranch *b_efatjets;                                    //!
  TBranch *b_mufatjets;                                   //!
  TBranch *b_HLT_mu50;                                    //!
  TBranch *b_HLT_mu26_ivarmedium;                         //!
  TBranch *b_HLT_e140_lhloose_nod0;                       //!
  TBranch *b_HLT_e26_lhtight_nod0_ivarloose;              //!
  TBranch *b_HLT_e120_lhloose;                            //!
  TBranch *b_HLT_mu20_iloose_L1MU15;                      //!
  TBranch *b_HLT_e24_lhmedium_L1EM20VH;                   //!
  TBranch *b_HLT_e60_lhmedium;                            //!
  TBranch *b_HLT_e60_lhmedium_nod0;                       //!
  TBranch *b_el_trigMatch_HLT_e60_lhmedium_nod0;          //!
  TBranch *b_el_trigMatch_HLT_e60_lhmedium;               //!
  TBranch *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;      //!
  TBranch *b_el_trigMatch_HLT_e120_lhloose;               //!
  TBranch *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose; //!
  TBranch *b_el_trigMatch_HLT_e300_etcut; //!
  TBranch *b_el_trigMatch_HLT_e140_lhloose_nod0;          //!
  TBranch *b_mu_trigMatch_HLT_mu50;                       //!
  TBranch *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;         //!
  TBranch *b_mu_trigMatch_HLT_mu26_ivarmedium;            //!
  TBranch *b_ljet_SubjetBScore_Top;                       //!
  TBranch *b_ljet_SubjetBScore_QCD;                       //!
  TBranch *b_ljet_SubjetBScore_Higgs;                     //!
  TBranch *b_ljet_Xbb202006_Top;                          //!
  TBranch *b_ljet_Xbb202006_QCD;                          //!
  TBranch *b_ljet_Xbb202006_Higgs;                        //!
  TBranch *b_ljet_m_Xbb2020v3_Top;                          //!
  TBranch *b_ljet_m_Xbb2020v3_QCD;                          //!
  TBranch *b_ljet_m_Xbb2020v3_Higgs;                        //!

  TBranch *b_jet_mass;                                    //!
  TBranch *b_tjet_raw_index;                              //!
  TBranch *b_ljet1_ghostVR_trkjet_m;                      //!
  TBranch *b_ljet1_ghostVR_trkjet_pt;                     //!
  TBranch *b_ljet1_ghostVR_trkjet_eta;                    //!
  TBranch *b_ljet1_ghostVR_trkjet_phi;                    //!
  TBranch *b_ljet1_ghostVR_trkjet_e;                      //!
  TBranch *b_ljet1_ghostVR_trkjet_DL1r;                   //!
  TBranch *b_ljet1_ghostVR_trkjet_DL1r_pu;                //!
  TBranch *b_ljet1_ghostVR_trkjet_DL1r_pc;                //!
  TBranch *b_ljet1_ghostVR_trkjet_DL1r_pb;                //!
  TBranch *b_ljet1_ghostVR_trkjet_truthflav;              //!
  TBranch *b_ljet1_ghostVR_trkjet_truthflavExtended;      //!
  TBranch *b_ljet1_ghostVR_trkjet_raw_index;              //!
  TBranch *b_ljet1_ghostVR_trkjet_index;              //!
  TBranch *b_ljet_ghostVR_indices;                        //!
  TBranch *b_ljet_ghostVR_indices_raw;                    //!

  //data
  TBranch *b_mu_original_xAOD;        //!
  TBranch *b_mu_actual_original_xAOD; //!

  ////////////////////////////////////////////////////////////////////
  // end of the copied part
  ////////////////////////////////////////////////////////////////////

  /*
   //Truth quantities (usually not present in nominal tree)
   double m_W1_had_pt, m_W1_had_eta, m_W1_had_phi, m_W1_had_m, m_W2_had_pt, m_W2_had_eta, m_W2_had_phi, m_W2_had_m, m_b_had_pt, m_b_had_eta, m_b_had_phi, m_b_had_m;
   double m_W1_lep_pt, m_W1_lep_eta, m_W1_lep_phi, m_W1_lep_m, m_W2_lep_pt, m_W2_lep_eta, m_W2_lep_phi, m_W2_lep_m, m_b_lep_pt, m_b_lep_eta, m_b_lep_phi, m_b_lep_m;
   int m_i_W1_had, m_i_W2_had, m_i_b_had, m_i_W1_lep, m_i_W2_lep, m_i_b_lep;
   bool m_is_full_had;
   bool m_is_dilep;

   //Truth branches (not present in nominal tree usually)
   TBranch *m_b_W1_had_pt, *m_b_W1_had_eta, *m_b_W1_had_phi, *m_b_W1_had_m, *m_b_W2_had_pt, *m_b_W2_had_eta, *m_b_W2_had_phi, *m_b_W2_had_m, *m_b_b_had_pt, *m_b_b_had_eta, *m_b_b_had_phi, *m_b_b_had_m; 
   TBranch *m_b_W1_lep_pt, *m_b_W1_lep_eta, *m_b_W1_lep_phi, *m_b_W1_lep_m, *m_b_W2_lep_pt, *m_b_W2_lep_eta, *m_b_W2_lep_phi, *m_b_W2_lep_m, *m_b_b_lep_pt, *m_b_b_lep_eta, *m_b_b_lep_phi, *m_b_b_lep_m; 
   TBranch *m_b_i_W1_had, *m_b_i_W2_had, *m_b_i_b_had, *m_b_i_W1_lep, *m_b_i_W2_lep, *m_b_i_b_lep;
   TBranch *m_b_is_full_had, *m_b_is_dilep;
   */

public:
  //constructor
  InputTree();
  //destructor
  ~InputTree();
  //initalize function (set branch adresses etc.)
  void initialize(TTree *tree, bool isData);
};

#endif //INPUT_TREE_H
