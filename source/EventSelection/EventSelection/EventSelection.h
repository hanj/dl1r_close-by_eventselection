#ifndef EVENTSELECTION_H
#define EVENTSELECTION_H

//cpp libraries

//ASG includes (eventloop,etc.)
#include "EventLoop/Algorithm.h"

//custom includes
//EventSelection includes
#include "EventSelection/InputTree.h"
// #include "EventSelection/Chi2_minimization.h"
#include "EventSelection/CrossSectionDatabase.h"
#include "EventSelection/sumOfWeightsProvider.h"
// #include "EventSelection/hadronizationProvider.h"
#include "EventSelection/HistSvc.h"
#include "EventSelection/ConfigStore.h"
// #include "EventSelection/MVATree.h"
// #include "EventSelection/AssignmentTree.h"

// //TopFakes includes
// #include "TopFakes/FakesWeights.h"
// #include "TopFakes/MMEfficiency.h"
// #include "TopFakes/MMEffSet.h"
// #include "TopFakes/MatrixUtils.h"

//ProbeJetTree includes
#include "ProbeJetTree/ProbeJetTree.h"

//ROOT includes
#include "TH1D.h"
#include "TLorentzVector.h"

//MVA includes

//Include the BTagging tools // noew not used.
// #include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
// #include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "PATInterfaces/SystematicSet.h"

class EventSelection : public EL::Algorithm
{

protected:
  InputTree m_input;                              //!
  TTree *m_tree;                                  //!
  // Chi2_minimization m_chi2;                       //!
  sumOfWeightsProvider *m_sumOfWeightsProvider;   //!
  // hadronizationProvider *m_hadronizationProvider; //!
  HistSvc *m_histSvc;                             //!
  HistNameSvc *m_histNameSvc;                     //!
  //list of weight variations for histSvc
  std::vector<HistSvc::WeightSyst> m_weightSysts; //!

  //probe-jet trees (split into collection years and lepton flavor)
  ProbeJetTree *m_probetree;         //!
  ProbeJetTree *m_probetree_el_2015;         //!
  ProbeJetTree *m_probetree_mu_2015;         //!
  ProbeJetTree *m_probetree_el_2016;         //!
  ProbeJetTree *m_probetree_mu_2016;         //!
  ProbeJetTree *m_probetree_el_2017;         //!
  ProbeJetTree *m_probetree_mu_2017;         //!
  ProbeJetTree *m_probetree_el_2018;         //!
  ProbeJetTree *m_probetree_mu_2018;         //!
  ProbeJetTree *m_boosted_probetree_el_2015; //!
  ProbeJetTree *m_boosted_probetree_mu_2015; //!
  ProbeJetTree *m_boosted_probetree_el_2016; //!
  ProbeJetTree *m_boosted_probetree_mu_2016; //!
  ProbeJetTree *m_boosted_probetree_el_2017; //!
  ProbeJetTree *m_boosted_probetree_mu_2017; //!
  ProbeJetTree *m_boosted_probetree_el_2018; //!
  ProbeJetTree *m_boosted_probetree_mu_2018; //!

  //MVATree
  // MVATree *m_MVATree;

  //AssignmentTree? what is this?
  // AssignmentTree *m_AssignmentTree;

  //cutflow histograms
  TH1D *m_cf_15_el_mc; //!
  TH1D *m_cf_15_mu_mc; //!
  TH1D *m_cf_16_el_mc; //!
  TH1D *m_cf_16_mu_mc; //!
  TH1D *m_cf_17_el_mc; //!
  TH1D *m_cf_17_mu_mc; //!
  TH1D *m_cf_18_el_mc; //!
  TH1D *m_cf_18_mu_mc; //!

  TH1D *m_cf_15_el_dat; //!
  TH1D *m_cf_15_mu_dat; //!
  TH1D *m_cf_16_el_dat; //!
  TH1D *m_cf_16_mu_dat; //!
  TH1D *m_cf_17_el_dat; //!
  TH1D *m_cf_17_mu_dat; //!
  TH1D *m_cf_18_el_dat; //!
  TH1D *m_cf_18_mu_dat; //!

  // BTaggingEfficiencyTool *m_bEff; //!
  // BTaggingSelectionTool *m_bSel;  //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

public:
  // this is a standard constructor
  EventSelection();
  ~EventSelection();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob(EL::Job &job) override;
  virtual EL::StatusCode fileExecute() override;
  virtual EL::StatusCode histInitialize() override;
  virtual EL::StatusCode changeInput(bool firstFile) override;
  virtual EL::StatusCode initialize() override;
  virtual EL::StatusCode execute() override;
  virtual EL::StatusCode postExecute() override;
  virtual EL::StatusCode finalize() override;
  virtual EL::StatusCode histFinalize() override;
  virtual EL::StatusCode executePreEvtSel() { return EL::StatusCode::SUCCESS; };

  bool passTrigger();
  int collectionYear();
  int checkElectronMatch();
  int checkMuonMatch();
  bool getLepton();
  bool getMET();
  bool getJets();
  bool getLargeRJets();
  bool getVRJets();
  bool getAssociatedVR();
  std::vector<TLorentzVector> m_AssociatedVR;
//NINI
  std::vector<float>  *weight_perAssociatedVR_DL1r85;
  std::vector<bool>  *isVHbbTagged_perAssociatedVR;
  std::vector<std::vector<float>> *weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_B_up;
  std::vector<std::vector<float>> *weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_B_down;
  std::vector<std::vector<float>> *weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_C_up;
  std::vector<std::vector<float>> *weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_C_down;
  std::vector<std::vector<float>> *weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_up;
  std::vector<std::vector<float>> *weight_perAjet_trackjet_bTagSF_DL1r_85_eigenvars_Light_down;
  std::vector<float>   *weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_up;
  std::vector<float>   *weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_down;
  std::vector<float>   *weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_up;
  std::vector<float>   *weight_perAjet_trackjet_bTagSF_DL1r_85_extrapolation_from_charm_down;
  std::vector<float> *Atjet_DL1r;
  bool passPreselection();
  bool passBoostedPreselection();
  double getMwt();
  double getEventWeight();
  EL::StatusCode fillHistograms();
  double getJetDrMin(size_t ref);
  void fillCutflow(int stage);
  void fillPrecutChi2Histos();
  double getJetsDeltaR(int jet_index);
  int getBoostedLepJet();
  // int getBoostedVRLepJet();
  // int getBoostedVRHadJet();
  int getBoostedTopJet();

  void setConfig(ConfigStore *config)
  {
    std::cout << "config file is going to be set" << config << std::endl;
    m_config = config;
    //print config
    m_config->print();
  };

  double get_mlj();
  double get_dR_lj_min();
  double get_Lep_d0();
  double get_lep_topoetcone();
  double get_lep_ptvarcone();
  double get_lep_new_iso();
  bool get_isTightLep();
  EL::StatusCode fillBoostedProbeJetTree();
  // void fillBoostedProbeJetTree();
  // void fill_MVA_control_plots();
  // void fill_chi2_controlPlots();

  // void ttbar_pt_correction();
  // void read_in_correction_ratios();
  // void ttbar_pt_binwise_correction();

  // void derive_chi2_quantities_from_truth();
  // double getdRIdxClose(TLorentzVector obj, double &idx, int iblep, int ibhad, int iW1, int iW2);
  // double getdRIdxCloseB(TLorentzVector obj, double &idx, int iblep, int ibhad, int iW1, int iW2);
  // double getdRIdxCloseNB(TLorentzVector obj, double &idx, int iblep, int ibhad, int iW1, int iW2);

  // void fill_MVA_tree();
  // void fill_assignment_tree();

  // void pt_binwise_correction_for_mva();

  // void initializeBTaggingTools();

  std::string getNameFromDSID(int dsid);
  std::string getTruthLabel(int* truths, int nVR);


protected:
  // I honestly don't know why this has to be below the function...

  // pointers to the current variation of objects
  // can be directly accessed from fill functions,
  // but it is saver to use the result of a selection class
  // (for consistent pre-selection, pT-sorting, ...)
  ConfigStore *m_config;

private:
  long int m_eventCounter; //!

  TLorentzVector m_lepton;             //!
  std::vector<TLorentzVector> m_jets;  //!
  std::vector<bool> m_jets_isTagged;   //!
  std::vector<TLorentzVector> m_ljets; //!
  // std::vector<bool> m_ljets_isTagged;  //!
  TLorentzVector m_met;                //!

  std::vector<TLorentzVector> m_VRjets;  //!L
  // std::vector<bool> m_VRjets_isTagged;   //!L



  int m_elTrigMatch; //!
  int m_muTrigMatch; //!

  //variables needed for the chi2 minimization
  // int m_jetIndex_q1_W;          //! //useless for boosted large-R jet calib.
  // int m_jetIndex_q2_W;          //! //useless for boosted large-R jet calib.
  // int m_jetIndex_b_had;         //! //useless for boosted large-R jet calib.
  // int m_jetIndex_b_lep;         //! //useless for boosted large-R jet calib.
  int m_jetIndex_boosted_top;   //!
  // int m_jetIndex_boosted_b_had; //! //useless for large-R jet calib.
  int m_jetIndex_boosted_b_lep; //!
  int m_probeIndex;             //!

  // int m_VRjetIndex_boosted_b_had; //!L
  // int m_VRjetIndex_boosted_b_lep; //!L

  // double m_chi2ming1H;    //!
  // double m_chi2ming1L;    //!
  // double m_chi2min_total; //!

  // bool m_chi2_ok; //!

  double m_eventWeight; //!

  // unsigned int m_klSolIndex; //!

  //correction ratios for pT shape? what is this??
  // std::vector<double> m_v_2015_el;
  // std::vector<double> m_v_2015_mu;
  // std::vector<double> m_v_2016_el;
  // std::vector<double> m_v_2016_mu;
  // std::vector<double> m_v_2017_el;
  // std::vector<double> m_v_2017_mu;
  // std::vector<double> m_v_2018_el;
  // std::vector<double> m_v_2018_mu;

  //pt correction factors for the mva training
  // std::vector<double> m_corr_mva;

  int m_isSignal; //!
  int m_sample;   //!

  // this is needed to distribute the algorithm to the workers
  ClassDefOverride(EventSelection, 1);
};

#endif // ifndef EVENTSELECTION_H
