#ifndef CROSS_SECTION_DATABASE_H
#define CROSS_SECTION_DATABASE_H

#include <string>
#include <unordered_map>

class CrossSectionDatabase
{
 public:
	struct Entry
	{
		int dsid;
		std::string name;
                double events_mc15;
	        double events_mc16a;
	        double events_mc16c;
	        double events_mc16d;
	        double events_mc16e;
                double sampleLumi;
		double amiXs;
                double filtEff;
		std::string higherOrderXs; // can be "Undefined", therefore usage of string
		double kFactor;
		double xs;
	};

	std::unordered_map<int, Entry> database;

	static CrossSectionDatabase& instance()
	{
		static CrossSectionDatabase singleton;
		return singleton;
	}

	void Load(std::string& path);

	double GetEffectiveXs(int dsid) const
	{
		if (dsid == cachedDsid){
			return cachedXs;
}

		if (database.find(dsid) == database.end()){
			return -1;
}
		const auto& entry = database.at(dsid);
		cachedDsid = entry.dsid;
		cachedXs = entry.xs;

		return entry.xs;
	}
        
private:
	CrossSectionDatabase() = default;
	//to be a proper singleton class one also needs to overwrite the copy and assignment operator
	CrossSectionDatabase( const CrossSectionDatabase& other);
	CrossSectionDatabase & operator = (const CrossSectionDatabase& other);

	void LoadFile(const std::string& file);

	mutable int cachedDsid = -1;
        mutable int cachedDsid2 = -1;
	mutable double cachedXs = -1;
        mutable int cachedNevents = -1;
};

#endif //CROSS_SECTION_DATABASE_H
