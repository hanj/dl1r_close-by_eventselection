#include <EventLoop/DirectDriver.h>
#include <EventLoop/CondorDriver.h>
#include <EventLoop/Job.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>

void retrieve_jobs (const std::string& submitDir)
{
  // Retrieve the job, waiting for it, if it is not finished
  EL::Driver::wait (submitDir);
}
