# Event selection code 

## Before running the code <a name="beforeRun"></a>

The major configurations of the code are specified using a ```.cfg``` file that, per default can be found via ```EventSelection/data/selectEvents.cfg```. Inside the file, all important settings are set (how many events to run over, sample directory, luminosity, b-tagging working point etc). In order to be able to run, a ```sumOfWeightsFile``` has to be created for the input nTuples one wants to run over (to have the correct event weights). This can be done using the python script ```EventSelection/scripts/count_Nentry_SumOfWeight.py``` in the directory of your samples. It will automatically scan the whole directory and calculate the sum of MC weights for the various processes. The file has to be correctly linked in the config file afterwards. 

## Running the code <a name="running"></a>

After everything is specified in the config file, the code is compiled and the working environment is sourced, please go into the ```run``` folder and start the event selection program with 
```
selectEvents
```
The output is then safed in a directory called ```submitDir```. Make sure this does not already exist before running the code. To run over the multiple MC campaigns, a script should make your life easier. For this, just go in the ```scripts``` folder and run the command:
```
cd source/EventSelection/scripts/
python runSelection.py -mc16 <mcp> -t <type> -o <output>
``` 
where:
* ```<mcp>``` is ```a```, ```d``` or ```e``` depending on the MC campaign to run 
* ```<type>``` is either ```b``` or ```r``` for boosted and resolved respectively
* ```<output>``` is the folder in which the outputs and batch jobs will be stored

Note that you can have more options and details using ```python ../source/EventSelection/scripts/runSelection.py --help```. This script will modify the template config file ```../source/EventSelection/data/selectEvents-skeleton.cfg``` according to your needed. So, make sure the latter contains the options you want.

**Batch system support:**

The code can be sent to a batch system to distribute the samples over multiple cores and save time. So far, only **torque** and **condor** batch systems are supportet. If different drivers are needed, please contact Brian Moser (brian.moser@cern.ch).

## Resubmit jobs 

Once alll jobs have run, it is possible to check the outputs are all here and resubmit the missing jobs if needed. From the ```scripts``` folder, the command is:

```
root -b -q '$ROOTCOREDIR/scripts/load_packages.C' 'resubmit_jobs.cxx("submitDir/")'
```

In case no jobs had to be resubmitted, a message will be prompted to say so. After the jobs are run, 

## Merge the outputs for all samples

The outputs of the jobs are stored in the `submitDir/fetch` folder (one output for each job). It is however possible to merge the outputs for each of the sub-samples using the command:

```
root -b -q '$ROOTCOREDIR/scripts/load_packages.C' 'retrieve_jobs.cxx("submitDir/")'
```

Another script has been written to merge the outputs to be ready for the next step. This script is in the ```scripts``` folder and can be executed as:

```
python merge_output.py -i <input> -o <output> -t <ttbar>
```
where:
* ```<input>``` is the path containing all of the output files from the jobs (i.e. the target of the ```-o``` option in the previous step)
* ```output``` is the name of the merged output file
* ```<ttbar>``` is the comma-separated list of ttbar modelling uncertainties (that name of the systematics should follow the name of the output files of the jobs)

## How to modify the code? <a name="modify"></a>
In this section, a few examples are given on how to modify the existing code. 

### Adding additional validation plots <a name="adding"></a>
This can be done in ```source/EventSelection/Root/EventSelection.cxx``` by using the handy ```BookFillHist``` function (this automatically creates and fills separate histograms for all specified regions of phase space).

```
m_histSvc->BookFillHist(std::string <name>, int <number of bins>, double <lower limit>, double <upper limit>, double <value to fill>, double <event weight>);
```

